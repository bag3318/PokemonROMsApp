import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import NumberValidators from 'src/app/validators/number.validators';
import UrlValidators from 'src/app/validators/url.validators';

@Component({
  selector: 'app-add-rom',
  templateUrl: './add-rom.component.html',
  styleUrls: ['./add-rom.component.scss']
})
export class AddRomComponent implements OnInit {
  @Output() addRom: EventEmitter<any> = new EventEmitter()
  private isProblemAppend: boolean = false;
  private regExs: [RegExp, string] = [
    new RegExp(/^(?:([0-9]{4}|[0-9]{2})[-]([0][1-9]|[1][0-2])[-]([0][1-9]|[1|2][0-9]|[3][0|1]))$/),
    '[0-9]{1,3}'
  ];
  private maxLengths: number[] = [23, 46, 50, 400, 8000];
  private pokemonROMsForm: FormGroup = new FormGroup({
    gameName: new FormControl('', [Validators.required, Validators.maxLength(this.maxLengths[1])]),
    generation: new FormControl('', [
      Validators.required,
      Validators.min(1),
      Validators.max(8),
      NumberValidators.isNotNumber
    ]),
    region: new FormControl('', [Validators.required, Validators.maxLength(this.maxLengths[0])]),
    dateReleased: new FormControl('', [Validators.required, Validators.pattern(this.regExs[0])]),
    platform: new FormControl('', [Validators.required, Validators.maxLength(this.maxLengths[0])]),
    websiteUrl: new FormControl('', [
      Validators.required,
      Validators.maxLength(this.maxLengths[3]),
      UrlValidators.isNotUrl
    ]),
    description: new FormControl('', [Validators.required, Validators.maxLength(this.maxLengths[4])]),
    coverArtImageUrl: new FormControl('', [
      Validators.required,
      Validators.maxLength(this.maxLengths[3]),
      UrlValidators.isNotUrl
    ]),
    fileName: new FormControl('', [Validators.required, Validators.maxLength(this.maxLengths[2])]),
    downloadLink: new FormControl('', [
      Validators.required,
      Validators.maxLength(this.maxLengths[3]),
      UrlValidators.isNotUrl
    ])
  });

    
  get GameName(): AbstractControl {
    let getGameName: AbstractControl = this.pokemonROMsForm.get('gameName');
    return getGameName;
  }

  get Generation(): AbstractControl {
    let getGeneration: AbstractControl = this.pokemonROMsForm.get('generation');
    return getGeneration;
  }

  get Region(): AbstractControl {
    let getRegion: AbstractControl = this.pokemonROMsForm.get('region');
    return getRegion;
  }

  get DateReleased(): AbstractControl {
    let getDateReleased: AbstractControl = this.pokemonROMsForm.get('dateReleased');
    return getDateReleased;
  }

  get WebsiteUrl(): AbstractControl {
    let getWebsiteUrl: AbstractControl = this.pokemonROMsForm.get('websiteUrl');
    return getWebsiteUrl;
  }

  get Description(): AbstractControl {
    let getDescription: AbstractControl = this.pokemonROMsForm.get('description');
    return getDescription;
  }

  get CoverArtImageUrl(): AbstractControl {
    let getCoverArtImageUrl: AbstractControl = this.pokemonROMsForm.get('coverArtImageUrl');
    return getCoverArtImageUrl;
  }

  get FileName(): AbstractControl {
    let getFileName: AbstractControl = this.pokemonROMsForm.get('fileName');
    return getFileName;
  }

  get DownloadLink(): AbstractControl {
    let getDownloadLink: AbstractControl = this.pokemonROMsForm.get('downloadLink');
    return getDownloadLink;
  }

  get Platform(): AbstractControl {
    let getPlatform: AbstractControl = this.pokemonROMsForm.get('platform');
    return getPlatform;
  }

  constructor() { }

  ngOnInit() {
  }
  onSubmit() {
    let gameData = {
      id: 0,
      gameName: this.GameName.value,
      generation: parseInt(this.Generation.value.toString(), 10),
      region: this.Region.value,
      dateReleased: this.DateReleased.value.toString(),
      description: this.Description.value,
      platform: this.Platform.value,
      websiteUrl: this.WebsiteUrl.value,
      coverArtImageUrl: this.CoverArtImageUrl.value,
      romFile: {
        downloadLink: this.DownloadLink.value,
        fileName: this.FileName.value
      }
    };
    console.log(gameData);

  this.addRom.emit(gameData);
  }

    private addRomBtnDisabled(): boolean {
    let disabled: boolean = ((this.pokemonROMsForm.invalid && (this.pokemonROMsForm.dirty || this.pokemonROMsForm.touched)) || (this.GameName.value === '' && this.Generation.value === '' && this.Region.value === '' && this.DateReleased.value === '' && this.Description.value === '' && this.FileName.value === '' && this.DownloadLink.value === '' && this.CoverArtImageUrl.value === '' && this.Platform.value === '' && this.WebsiteUrl.value === ''));
    return disabled;
  }

  private updateRomBtnDisabled(): boolean {
    let disabled: boolean = ((this.pokemonROMsForm.invalid && (this.pokemonROMsForm.dirty || this.pokemonROMsForm.touched)) || (this.GameName.value === '' && this.Generation.value === '' && this.Region.value === '' && this.DateReleased.value === '' && this.Description.value === '' && this.FileName.value === '' && this.DownloadLink.value === '' && this.CoverArtImageUrl.value === '' && this.Platform.value === '' && this.WebsiteUrl.value === ''));
    return disabled;
  }

} 

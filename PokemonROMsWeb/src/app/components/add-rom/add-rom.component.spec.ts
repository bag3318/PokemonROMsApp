import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRomComponent } from './add-rom.component';

describe('AddRomComponent', () => {
  let component: AddRomComponent;
  let fixture: ComponentFixture<AddRomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { RomsApiService } from 'src/app/services/roms-api.service';
import { decode } from 'he';

@Component({
  selector: 'app-roms',
  templateUrl: './roms.component.html',
  styleUrls: ['./roms.component.scss']
})
export class RomsComponent implements OnInit {
  roms: Rom[] = [];
  isConnected: boolean = false;
  connectionMsgClasses: string = 'alert alert-primary alert-msg-small center';
  connectionMsg: string = 'Loading table data from database...';
  constructor(private romsApiService: RomsApiService) { }

  ngOnInit() {
    this.romsApiService.getRoms().subscribe(roms => {
      decodeAllData(roms);
      this.roms = roms;
    }, error => {
      this.isConnected = false;
      console.error(error);
      // alert('An error has occured while trying to retrieve the ROM data.\r\nCheck the console for more info.');
      this.connectionMsgClasses = 'alert alert-danger alert-msg-small center';
      this.connectionMsg = `
        No table data to show.
        <br/>
        Connection to database may have failed.
        <br/>
        You may check the console for more information.
      `;
    });
  }
  private dataExists(): boolean {
    return this.roms.length === 0 ? false : true;
  }
  deleteRom(rom: Rom) {
    this.roms = this.roms.filter(r => r.gameId !== rom.gameId);
    this.romsApiService.deleteRom(rom).subscribe();
  }
  addRom(rom: GameData) {
    this.romsApiService.addRom(rom).subscribe(rom => {
      this.roms.unshift(rom);
    });
  }

}

let excludedProperties: string[] = ['downloadLink', 'coverArtImageUrl', 'websiteUrl'];

function decodeAllData(data: Rom[]): void {
  var i: number;
  for (i = 0; i < data.length; i++) {
    var info: string;
    for (info in data[i]) {
      if (data[i].hasOwnProperty(info)) {
        excludedProperties.forEach((excludedProperty: string): void => {
          if (info !== excludedProperty) {
            data[i][info] = decode(data[i][info].toString());
          } else {
            data[i][info] = decodeURIComponent(data[i][info]);
          }
        });
      }
    }
  }
}

export default decodeAllData;
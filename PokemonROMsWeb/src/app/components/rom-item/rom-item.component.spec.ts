import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RomItemComponent } from './rom-item.component';

describe('RomItemComponent', () => {
  let component: RomItemComponent;
  let fixture: ComponentFixture<RomItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RomItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RomItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-rom-item',
  templateUrl: './rom-item.component.html',
  styleUrls: ['./rom-item.component.scss']
})
export class RomItemComponent implements OnInit {
  @Input() rom: Rom;
  @Output() deleteRom: EventEmitter<Rom> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onDelete(rom) {
    this.deleteRom.emit(rom);
  }
}

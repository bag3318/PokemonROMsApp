import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': 'http://localhost:4200',
    'Access-Control-Allow-Methods': 'POST, OPTIONS',
    'Access-Control-Allow-Headers': 'Accept, Content-Type',
    'Content-Type': 'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class RomsApiService {
  romsUrl: string = 'https://localhost:44389/api/PokemonROMs';
  
  constructor(private http:HttpClient) { }
  getRoms(): Observable<Rom[]> {
    return this.http.get<Rom[]>(this.romsUrl);
  }
  deleteRom(rom: Rom): Observable<any> {
    return this.http.delete<any>(`${this.romsUrl}/${rom.gameId}`);
  }
  addRom(rom: GameData):Observable<Rom> {
    return this.http.post<Rom>(this.romsUrl, rom, httpOptions);
  }
}

import { TestBed } from '@angular/core/testing';

import { RomsApiService } from './roms-api.service';

describe('RomsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RomsApiService = TestBed.get(RomsApiService);
    expect(service).toBeTruthy();
  });
});

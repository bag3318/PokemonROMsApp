import { AbstractControl, ValidationErrors } from '@angular/forms';

export default class UrlValidators {
  public static isNotUrl(control: AbstractControl): ValidationErrors | null {
    const urlRegEx: RegExp = new RegExp(/^(?:https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=()!]*))$/, 'i');
    if ((control.value as string) === null) {
      (control.value as string) = '';
    }
    if (!(control.value as string).match(urlRegEx)) {
      return {
        isNotUrl: true
      };
    }
    return null;
  }
}

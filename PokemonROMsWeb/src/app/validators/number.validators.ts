import { AbstractControl, ValidationErrors } from '@angular/forms';

export default class NumberValidators {
  public static isNotNumber(control: AbstractControl): ValidationErrors | null {
    if (!Number.isInteger(parseInt(control.value as string, 10))) {
      return {
        isNotNumber: true
      };
    }
    return null;
  }
}

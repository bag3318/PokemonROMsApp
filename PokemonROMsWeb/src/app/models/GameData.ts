interface GameData {
  id: number;
  gameName: string;
  generation: number;
  region: string;
  dateReleased: string|Date;
  description: string;
  platform: string;
  websiteUrl: string;
  coverArtImageUrl: string;
  romFile: IROMFile;
}
interface IROMFile {
    gameId?: string|undefined;
    downloadLink: string;
    fileName: string;
  }
  
interface Rom {
    gameId: number;
    gameName: string;
    generation: number;
    region: string;
    dateReleased: string|Date;
    description: string;
    platform: string;
    websiteUrl: string;
    coverArtImageUrl: string;
    downloadLink: string;
    fileName: string;
  }
/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

// required module definitions
declare module 'he'; // html decoding library
declare module 'sprintf-js'; // print-string formatter
declare module 'typescript-string-operations'; // post-string-interpolation formatter

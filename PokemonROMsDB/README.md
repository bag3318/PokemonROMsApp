[comment]: # (BEGIN README.md)

# PokemonROMsDB

## About

This part of the repository is the database.

This database is situated on the backend/server-side of this application.
It acts as a storage and querying space for all of the ROM data.

The database is running on Microsoft's **[SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express "Download Microsoft SQL Server Express")** database.

This database is also relational, which means it stores data in tables.

Additionally, the language used for querying this database is called **SQL**, or structured query langauge. It is a very simple looking language but has a lot of quirks to it.

SQL methods and syntax very by database provider.

## Security

Not much security is implemented on this database; however, it does use **stored procedures**, which is a method of querying in SQL that uses parameters as part of the query statement.

The main reason stored procedures were used is because of the ability to rollback a transaction if the querying process fails. This way, bad data or incomplete data won't contaminate the tables in the database.

## Development Tools

In norder to manage this database, I used Microsoft's **[SQL Server Managment Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017 "Download SQL Server Managment Studio")**, which is an RDBMS (relational database managment system).

Additionaly, I used **[draw.io](https://about.draw.io/integrations/#integrations_offline "Download Draw.io diagramming software")** to make the DB schema for this database.

You can load the `PokemonROMsDBSchema.xml` file with [draw.io](https://www.draw.io/ "Free Online Diagram Software") to view the schema visually.

To open this Database, open the `PokemonROMsDB.ssmssln` file with **SQL Server Managment Studio**.

## Database Script

The file named `PokemonROMsDB.sql` contains the full script for the `PokemonROMsDB` database. Just run it with SQL Server and the database will be created.

________

> _**Enjoy!**_

[comment]: # "END README.md"

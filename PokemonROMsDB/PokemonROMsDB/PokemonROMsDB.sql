/***************************************
 * PokemonROMsDB SQL Script            *
 * Made By: bag33188                   *
 * DB System: MS SQL Server            *
 * RDBMS: SQL Server Management Studio *
 * ----------------------------------- *
 * SQL Server Authentication           *
 * Server: BAG33188\\SQLEXPRESS        * 
 * User Name: BAG33188                 *
 * Password: 1988                      *
 ***************************************/

-- Begin

PRINT 'Started Running Script'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `PokemonROMsDB` Database...'
GO

IF (DB_ID('PokemonROMsDB') IS NULL)
	CREATE DATABASE PokemonROMsDB
ELSE
BEGIN
	PRINT CHAR(13) + CHAR(10)
	PRINT 'Database `PokemonROMsDB` Already Exists. Dropping Current `PokemonROMsDB` Database...'
	USE master
	ALTER DATABASE PokemonROMsDB SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	--ALTER DATABASE PokemonROMsDB SET OFFLINE WITH ROLLBACK IMMEDIATE
	--ALTER DATABASE PokemonROMsDB SET ONLINE
	DROP DATABASE PokemonROMsDB
	PRINT 'Successfully Dropped `PokemonROMsDB` Database!'
	PRINT CHAR(13) + CHAR(10)
	CREATE DATABASE PokemonROMsDB
END
GO

PRINT 'Finished Creating `PokemonROMsDB` Database!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Begin Using `PokemonROMsDB` Database...'
GO

USE PokemonROMsDB
GO

PRINT 'Now using `PokemonROMsDB` Database!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating Tables...'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `GameData` Table...'
GO

CREATE TABLE dbo.GameData (
	ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	GameName NVARCHAR(46) NOT NULL,
	Generation TINYINT NOT NULL, 
	Region NVARCHAR(23) NOT NULL,
	DateReleased DATE NOT NULL,
	[Platform] NVARCHAR(23) NOT NULL,
	WebsiteURL NVARCHAR(400) NOT NULL,
	[Description] NVARCHAR(MAX) NOT NULL, -- MAX = 4000
	CoverArtImageURL NVARCHAR(400) NOT NULL
)
GO

PRINT 'Finished Creating `GameData` Table!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `ROMFiles` Table...'
GO

CREATE TABLE dbo.ROMFiles (
	ID INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	GameID INT NOT NULL,
	[FileName] NVARCHAR(50) NOT NULL, 
	DownloadLink NVARCHAR(400) NOT NULL, 
	CONSTRAINT [FK__ROMFiles__GameData] FOREIGN KEY (GameID) REFERENCES dbo.GameData(ID)
)
GO

PRINT 'Finished Creating `ROMFiles` Table!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Finished Creating All Tables!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `PokemonROMs` View...'
GO

CREATE VIEW dbo.PokemonROMs AS
SELECT 
	r.GameID, 
	g.GameName, 
	g.Generation, 
	g.Region,
	g.DateReleased, 
	g.[Description], 
	g.[Platform], 
	g.WebsiteURL, 
	g.CoverArtImageURL, 
	r.DownloadLink,
	r.[FileName]
FROM dbo.GameData AS g
INNER JOIN dbo.ROMFiles AS r
ON g.ID = r.GameID
GO

PRINT 'Finished Creating `PokemonROMs` View!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating Stored Procedures...'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spSelectPokemonROM` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spSelectPokemonROM
@Id INT
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			SELECT * FROM dbo.PokemonROMs WHERE GameID = @Id
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
		IF @@ROWCOUNT > 0
			ROLLBACK TRAN
	END CATCH
END
GO

PRINT 'Finished Creating `spSelectPokemonROM` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spInsertPokemonROM` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spInsertPokemonROM
@GameName NVARCHAR(188),
@Generation TINYINT,
@Region NVARCHAR(88),
@DateReleased DATE,
@Platform NVARCHAR(120),
@WebsiteURL NVARCHAR(1988),
@Description NVARCHAR(MAX),
@CoverArtImageUrl NVARCHAR(1988),
@FileName NVARCHAR(200),
@DownloadLink NVARCHAR(1988)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			INSERT INTO dbo.GameData (GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) 
			VALUES (@GameName, @Generation, @Region, @DateReleased, @Platform, @WebsiteURL, @Description, @CoverArtImageUrl); 
			INSERT INTO dbo.ROMFiles (GameID, [FileName], DownloadLink) 
			VALUES (dbo.fnGetLatestGameDataID(), @FileName, @DownloadLink)
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
		IF @@ROWCOUNT > 0
			ROLLBACK TRAN
	END CATCH
END
GO

PRINT 'Finished Creating `spInsertPokemonROM` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spUpdatePokemonROM` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spUpdatePokemonROM
@Id INT,
@GameName NVARCHAR(188),
@Generation TINYINT,
@Region NVARCHAR(88),
@DateReleased DATE,
@Platform NVARCHAR(120),
@WebsiteURL NVARCHAR(1988),
@Description NVARCHAR(MAX),
@CoverArtImageUrl NVARCHAR(1988),
@FileName NVARCHAR(200),
@DownloadLink NVARCHAR(1988)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE dbo.GameData
			 SET GameName = @GameName, Generation = @Generation, Region = @Region, DateReleased = @DateReleased, [Platform] = @Platform, WebsiteURL = @WebsiteURL, [Description] = @Description, CoverArtImageURL = @CoverArtImageUrl
			WHERE ID = @Id
			UPDATE dbo.ROMFiles 
			SET [FileName] = @FileName, DownloadLink = @DownloadLink
			WHERE GameID = @Id 
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
		IF @@ROWCOUNT > 0
			ROLLBACK TRAN
	END CATCH
END
GO

PRINT 'Finished Creating `spUpdatePokemonROM` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spDeletePokemonROM` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spDeletePokemonROM
@Id INT
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			DELETE FROM dbo.ROMFiles WHERE GameID = @Id
			DELETE FROM dbo.GameData WHERE ID = @Id
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
	END CATCH
END
GO

PRINT 'Finished Creating `spDeletePokemonROM` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spDeleteAllPokemonROMs` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spDeleteAllPokemonROMs
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			ALTER TABLE dbo.ROMFiles DROP CONSTRAINT [FK__ROMFiles__GameData]
			TRUNCATE TABLE dbo.GameData
			TRUNCATE TABLE dbo.ROMFiles
			ALTER TABLE dbo.ROMFiles WITH CHECK ADD CONSTRAINT [FK__ROMFiles__GameData] FOREIGN KEY(GameID)
			REFERENCES dbo.GameData (ID)
			ALTER TABLE dbo.ROMFiles CHECK CONSTRAINT [FK__ROMFiles__GameData]
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
	END CATCH
END
GO

PRINT 'Finished Creating `spDeleteAllPokemonROMs` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spInsertCorePokemonROMs` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spInsertCorePokemonROMs
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			EXEC dbo.spDeleteAllPokemonROMs;
			SET IDENTITY_INSERT dbo.GameData ON 
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (1, N'Pok&#xE9;mon Red', 1, N'Kanto', N'1998-09-28', N'Game Boy', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-red-version-and-pokemon-blue-version/', N'Pok&#xE9;mon Red and Pok&#xE9;mon Blue are now available for systems in the Nintendo 3DS family! The games remain true to the originals, complete with monochromatic pixel art and 4-bit background music, so you can feel as though you&#x27;re experiencing the games just as they were back then!

			These titles are compatible with wireless communication for the first time ever. In the days of the Game Boy, players had to use a Link Cable to connect with friends, but these games use the Nintendo 3DS system&#x27;s wireless capabilities to allow you to trade and battle Pok&#xE9;mon in just the same way. Pok&#xE9;mon Bank is also now compatible with Pok&#xE9;mon Red and Pok&#xE9;mon Blue, allowing you to transfer the Pok&#xE9;mon you&#x27;ve caught to the upcoming Pok&#xE9;mon Sun and Pok&#xE9;mon Moon video games!

			Using Pok&#xE9;mon Bank, you&#x27;ll be able to transfer Pok&#xE9;mon you&#x27;ve caught in the Nintendo 3DS Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow into your copy of Pok&#xE9;mon Sun or Pok&#xE9;mon Moon. Pok&#xE9;mon from Pok&#xE9;mon Omega Ruby, Pok&#xE9;mon Alpha Sapphire, Pok&#xE9;mon X, and Pok&#xE9;mon Y can also be brought into Pok&#xE9;mon Sun and Pok&#xE9;mon Moon in the same way.

			Visit the Pok&#xE9;mon Bank page for important details on the planned update to support Pok&#xE9;mon Sun and Pok&#xE9;mon Moon.

			Pok&#xE9;mon Red and Pok&#xE9;mon Blue introduce legions of gamers to the world of Kanto, where the likes of Charmander, Pikachu, and Mewtwo were first discovered. Through exciting exploration, battles, and trades, Trainers are able to access 150 Pok&#xE9;mon.

			You begin your journey in Pallet Town as a young boy. After a dangerous brush with wild Pok&#xE9;mon, Professor Oak teaches you how to capture Pok&#xE9;mon, and then sends you on your way as a fledgling Trainer. During your journey through Kanto, you must capture Pok&#xE9;mon to record their information in your Pok&#xE9;dex, as well as become a better Trainer by competing in Gyms scattered throughout the region. Once you&#x27;ve proven your mettle as a Pok&#xE9;mon Trainer, it&#x27;s time to take on the Elite Four, a crack group of Trainers that will put all of your learned skills to the test.

			Your journey will be far from easy. In addition to the many Trainers and wild Pok&#xE9;mon you&#x27;ll encounter along the way, you&#x27;ll also have to be watchful of Team Rocket, a despicable group of Pok&#xE9;mon thieves. Prevent Team Rocket from stealing rare Pok&#xE9;mon and stop their criminal ways!

			You won&#x27;t be able to catch every Pok&#xE9;mon in either Pok&#xE9;mon Red or Pok&#xE9;mon Blue; to collect every Pok&#xE9;mon, you&#x27;ll have to trade with friends via the Game Link&#x2122; Cable. With it, you can also take your team of faithful Pok&#xE9;mon into battle against your pals to see how well your team stacks up!

			There&#x27;s much to see and do in Pok&#xE9;mon Red and Pok&#xE9;mon Blue. Start your journey through Kanto and become a Master Trainer!', N'https://qdvruq.dm.files.1drv.com/y4mI6CrYuxzLhmHpcTv1f9kg-yyDdf4eqnlEizgYekNdIBf6fScADRDa2XVaAJQhmYytkCBKgZFBvTibMzJxKhj1yNV89dTjvRqflRVMta_Qfb7r1uotKTzROS3AdZaWLhVBPtB_Twxqon3GnzwEqxFnD33RRBEX0A1Lzr74lcu3r192LyKn3e8PLFCB6JoDPQ9vLgZScvKvBfat5ijikrBpw?width=898&height=890&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (2, N'Pok&#xE9;mon Blue', 1, N'Kanto', N'1998-09-28', N'Game Boy', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-red-version-and-pokemon-blue-version/', N'Pok&#xE9;mon Red and Pok&#xE9;mon Blue are now available for systems in the Nintendo 3DS family! The games remain true to the originals, complete with monochromatic pixel art and 4-bit background music, so you can feel as though you&#x27;re experiencing the games just as they were back then!

			These titles are compatible with wireless communication for the first time ever. In the days of the Game Boy, players had to use a Link Cable to connect with friends, but these games use the Nintendo 3DS system&#x27;s wireless capabilities to allow you to trade and battle Pok&#xE9;mon in just the same way. Pok&#xE9;mon Bank is also now compatible with Pok&#xE9;mon Red and Pok&#xE9;mon Blue, allowing you to transfer the Pok&#xE9;mon you&#x27;ve caught to the upcoming Pok&#xE9;mon Sun and Pok&#xE9;mon Moon video games!

			Using Pok&#xE9;mon Bank, you&#x27;ll be able to transfer Pok&#xE9;mon you&#x27;ve caught in the Nintendo 3DS Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow into your copy of Pok&#xE9;mon Sun or Pok&#xE9;mon Moon. Pok&#xE9;mon from Pok&#xE9;mon Omega Ruby, Pok&#xE9;mon Alpha Sapphire, Pok&#xE9;mon X, and Pok&#xE9;mon Y can also be brought into Pok&#xE9;mon Sun and Pok&#xE9;mon Moon in the same way.

			Visit the Pok&#xE9;mon Bank page for important details on the planned update to support Pok&#xE9;mon Sun and Pok&#xE9;mon Moon.

			Pok&#xE9;mon Red and Pok&#xE9;mon Blue introduce legions of gamers to the world of Kanto, where the likes of Charmander, Pikachu, and Mewtwo were first discovered. Through exciting exploration, battles, and trades, Trainers are able to access 150 Pok&#xE9;mon.

			You begin your journey in Pallet Town as a young boy. After a dangerous brush with wild Pok&#xE9;mon, Professor Oak teaches you how to capture Pok&#xE9;mon, and then sends you on your way as a fledgling Trainer. During your journey through Kanto, you must capture Pok&#xE9;mon to record their information in your Pok&#xE9;dex, as well as become a better Trainer by competing in Gyms scattered throughout the region. Once you&#x27;ve proven your mettle as a Pok&#xE9;mon Trainer, it&#x27;s time to take on the Elite Four, a crack group of Trainers that will put all of your learned skills to the test.

			Your journey will be far from easy. In addition to the many Trainers and wild Pok&#xE9;mon you&#x27;ll encounter along the way, you&#x27;ll also have to be watchful of Team Rocket, a despicable group of Pok&#xE9;mon thieves. Prevent Team Rocket from stealing rare Pok&#xE9;mon and stop their criminal ways!

			You won&#x27;t be able to catch every Pok&#xE9;mon in either Pok&#xE9;mon Red or Pok&#xE9;mon Blue; to collect every Pok&#xE9;mon, you&#x27;ll have to trade with friends via the Game Link&#x2122; Cable. With it, you can also take your team of faithful Pok&#xE9;mon into battle against your pals to see how well your team stacks up!

			There&#x27;s much to see and do in Pok&#xE9;mon Red and Pok&#xE9;mon Blue. Start your journey through Kanto and become a Master Trainer!', N'https://rnvruq.dm.files.1drv.com/y4mJaC3cSVhjAX1O-W3AALorVBr_z6cXP7v6mhIIlfL21hFWMnqSi7j7Y53UssvbnnoyOqyn39H9mSZDsSzHEK-c9oYL2oNzQB1Bq7a_KfhifgwEQkk7nrTsrlTeuIb6Y2WwaxQbKCFMQd-d92aFKkB5pBZCwLwamy1I6DEEDTPL6mwy24_xwj29LNLa7zV7z77-ewcpso4S4Gvg4H6WZFMmg?width=894&height=894&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (3, N'Pok&#xE9;mon Green (JP)', 1, N'Kanto', N'1996-02-27', N'Game Boy', N'https://www.pokemon.co.jp/game/other/gb-rg/summary.html', N'[TRANSLATED FROM JAPANESE]: &#x22;Pok&#xE9;mon red / green&#x22; is the first software of Pok&#xE9;mon. The hero who lives in Masala Town in Can Tho region gets one Pok&#xE9;mon from Dr. Orchid and goes on an adventure trip!
			There are 150 kinds of Pok&#xE9;mon in all &#x22;red / green&#x22;. &#x22;Red&#x22; and &#x22;green&#x22; have different rates of appearance of Pok&#xE9;mon, and there are Pok&#xE9;mons that only appear on one side. In order to collect a lot of Pok&#xE9;mon, it is necessary to exchange another version and Pok&#xE9;mon. Enjoy it with friends and family!
			&#x22;Pok&#xE9;mon red / green&#x22; can exchange communication with &#x22;blue&#x22; and &#x22;Pikachu&#x22;. Let&#x27;s enjoy the exchange of Pok&#xE9;mon with friends who have &#x22;blue&#x22; and &#x22;Pikachu&#x22;!
			Many Pok&#xE9;mon that can not be obtained in the sequel &#x22;Gold, Silver, Crystal Version&#x22; appear in &#x22;Red / Green&#x22;. Omnite revived from fossils, Kabuto. Legendary Thunder, Fire, Freezer. And Mewtwo is particularly precious. Absolutely catch it!', N'https://q9vruq.dm.files.1drv.com/y4mDJQ58bznliV4EKia9nOF20Bb1TJDxxAF2Kho-5Gt26k9ihKI7qLzk435U7_C33yKVffxpTHudkJgYyleSMHPZv_2OFeO92EBo2SAwH5G9oYo1Txve_vqFLkOB-FpCCFXLIvD9PSQ6YW4_Ms1XhcHLeiIx1DK5_kQJ9FTc-QI16EJXQSMbgZcQpYVc4mbyZTd_rONw4uiiIca-IfOfXErTA?width=479&height=599&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (4, N'Pok&#xE9;mon Yellow (Special Pikachu Edition)', 1, N'Kanto', N'1999-10-01', N'Game Boy', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-yellow-special-pikachu-edition/', N'Pok&#xE9;mon Yellow Version: Special Pikachu Edition is now available for systems in the Nintendo 3DS family! The game remains true to the original, complete with monochromatic pixel art and 4-bit background music, so you can feel as though you&#x27;re experiencing the game just as it was back then!

			This title is compatible with wireless communication for the first time ever. In the days of the Game Boy, players had to use a Link Cable to connect with friends, but this game uses the Nintendo 3DS system&#x27;s wireless capabilities to allow you to trade and battle Pok&#xE9;mon in just the same way. Pok&#xE9;mon Bank is also now compatible with Pok&#xE9;mon Yellow, allowing you to transfer the Pok&#xE9;mon you&#x27;ve caught to the upcoming Pok&#xE9;mon Sun and Pok&#xE9;mon Moon video games!

			Using Pok&#xE9;mon Bank, you&#x27;ll be able to transfer Pok&#xE9;mon you&#x27;ve caught in the Nintendo 3DS Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow into your copy of Pok&#xE9;mon Sun or Pok&#xE9;mon Moon. Pok&#xE9;mon from Pok&#xE9;mon Omega Ruby, Pok&#xE9;mon Alpha Sapphire, Pok&#xE9;mon X, and Pok&#xE9;mon Y can also be brought into Pok&#xE9;mon Sun and Pok&#xE9;mon Moon in the same way.

			Visit the Pok&#xE9;mon Bank page for important details on the planned update to support Pok&#xE9;mon Sun and Pok&#xE9;mon Moon.

			Building on the terrific success of Pok&#xE9;mon Red Version and Pok&#xE9;mon Blue Version, Pok&#xE9;mon Yellow Version returns Trainers to Kanto for more even more fun and adventure. Pok&#xE9;mon Yellow delivers a feature that hasn&#x27;t been duplicated in any other Pok&#xE9;mon game-Pikachu actually follows you around throughout your journey!

			The graphics for Pok&#xE9;mon Yellow are updated slightly from Pok&#xE9;mon Red and Pok&#xE9;mon Blue, and you can use innovative peripherals such as the Game Boy Printer, which allow you to print out stickers of your favorite Pok&#xE9;mon. Pok&#xE9;mon Yellow also introduces challenges and battle modes that let you compete in exciting ways.', N'https://ptvruq.dm.files.1drv.com/y4mLlq9sUWP3WerjkvRXvaSN9W3YabZpmQbjvK7oaEDt7pvpNpujfTI6_aoHnVxUG_O3mcQlaDSF3ltqgbmSd34DU4jH6DPLxBA7kcxGClqKL46o7LImo4-5QS2fOwXJLzANHolxfRdLU4Y3dpGaXoJwC3BYEmt9b5yEjmcCgf1X21mHrXQH4QHH57WjyMYst-vlKMvzX8aksmoqv01wFBcvg?width=894&height=894&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (5, N'Pok&#xE9;mon Gold', 2, N'Johto', N'2000-10-15', N'Game Boy Color', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-gold-version-and-pokemon-silver-version/', N'The classic games Pok&#xE9;mon Gold and Pok&#xE9;mon Silver are back! The two games return as Virtual Console titles for systems in the Nintendo 3DS family on September 22, 2017.

			Pok&#xE9;mon Gold and Pok&#xE9;mon Silver were released in Japan on November 21, 1999, as the second set of titles in the Pok&#xE9;mon series. These games, which were the first titles in the Pok&#xE9;mon series to be designed for the Game Boy Color, will be recreated in their Virtual Console versions so their screens appear just as they did on the Game Boy Color. Players will be able to enjoy playing them just as they remember playing them in earlier days.

			As with the Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition, these titles will be compatible with the wireless communication features of the Nintendo 3DS system. Players can look forward to Link Trades and Link Battles between Virtual Console versions of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver.

			In addition, these titles will also be compatible with the Time Capsule function, which allows players to Link Trade Pok&#xE9;mon between Pok&#xE9;mon Gold or Pok&#xE9;mon Silver and Pok&#xE9;mon Red, Pok&#xE9;mon Blue, or Pok&#xE9;mon Yellow: Special Pikachu Edition in Pok&#xE9;mon Centers within the games, as long as the species of Pok&#xE9;mon appeared in Pok&#xE9;mon Red, Pok&#xE9;mon Blue, or Pok&#xE9;mon Yellow: Special Pikachu Edition as well. Have the Pok&#xE9;mon that worked so hard for you in Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition keep battling by your side in Pok&#xE9;mon Gold and Pok&#xE9;mon Silver.

			These titles have also been slated to be compatible with Pok&#xE9;mon Bank, just as the Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition are. Through Pok&#xE9;mon Bank, you can bring the Pok&#xE9;mon that you catch in the Virtual Console editions of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver to Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon as well, which were announced today to be released on November 17, 2017. Look forward to future releases with more updates about when this compatibility can be expected and other details.

			One of the first mainstream RPG games to appear on the Game Boy Color, Pok&#xE9;mon Gold Version and Pok&#xE9;mon Silver Version continued to expand the Pok&#xE9;mon universe, adding 100 Pok&#xE9;mon, held items, and real-time events, thanks to an internal game clock.

			These major Pok&#xE9;mon releases take you on a journey through the Johto region, and for the first time, show your Pok&#xE9;mon appearing in glorious color on your Game Boy Color. All kinds of creative aspects are to be found in Pok&#xE9;mon Gold and Pok&#xE9;mon Silver, including genders and items that Pok&#xE9;mon can hold to improve their skills.

			Dozens of Pok&#xE9;mon join the action, including the trio of Legendary Pok&#xE9;mon Raikou, Entei, and Suicune. They&#x27;re always on the move, so you&#x27;ll need to be at the right place at the right time to catch any of them. Pok&#xE9;mon Gold and Pok&#xE9;mon Silver introduce new Pok&#xE9;mon types as well&#x2012;look out for Steel- and Dark-type Pok&#xE9;mon throughout your travels.

			Pok&#xE9;mon Egg groups expand the ways you can collect Pok&#xE9;mon. Leave two compatible Pok&#xE9;mon at the Pok&#xE9;mon Day Care and cross your fingers. If you&#x27;re lucky, an Egg may appear that will hatch into a new young Pok&#xE9;mon! Only some Pok&#xE9;mon will give you an Egg, and Pok&#xE9;mon are picky about which other Pok&#xE9;mon they get along with, so it will take plenty of experimenting to find a match!

			Check out Pok&#xE9;mon Gold and Pok&#xE9;mon Silver on the Virtual Console starting September 22, 2017.', N'https://pdvruq.dm.files.1drv.com/y4mnHohHnMpqDR_fw4rUx-hDvJxH0sQzMah1XwlRKJu6SMi3FHPKMoNKOaGnkI65HA-l5YwAeXb2cloMMNOietZ7o1zCw0-frzXnQXQTRTFeJAu4K-oR12xQIqBlk6iiwdfTk222Rb8872Zq2qq7SBAvND-_9frP-33exE1BKCXmyoXoPC-f8VGd0NPE-oW7odCl6z3ewWce5D-SbjS3-inTA?width=220&height=220&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (6, N'Pok&#xE9;mon Silver', 2, N'Johto', N'2000-10-15', N'Game Boy Color', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-gold-version-and-pokemon-silver-version/', N'The classic games Pok&#xE9;mon Gold and Pok&#xE9;mon Silver are back! The two games return as Virtual Console titles for systems in the Nintendo 3DS family on September 22, 2017.

			Pok&#xE9;mon Gold and Pok&#xE9;mon Silver were released in Japan on November 21, 1999, as the second set of titles in the Pok&#xE9;mon series. These games, which were the first titles in the Pok&#xE9;mon series to be designed for the Game Boy Color, will be recreated in their Virtual Console versions so their screens appear just as they did on the Game Boy Color. Players will be able to enjoy playing them just as they remember playing them in earlier days.

			As with the Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition, these titles will be compatible with the wireless communication features of the Nintendo 3DS system. Players can look forward to Link Trades and Link Battles between Virtual Console versions of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver.

			In addition, these titles will also be compatible with the Time Capsule function, which allows players to Link Trade Pok&#xE9;mon between Pok&#xE9;mon Gold or Pok&#xE9;mon Silver and Pok&#xE9;mon Red, Pok&#xE9;mon Blue, or Pok&#xE9;mon Yellow: Special Pikachu Edition in Pok&#xE9;mon Centers within the games, as long as the species of Pok&#xE9;mon appeared in Pok&#xE9;mon Red, Pok&#xE9;mon Blue, or Pok&#xE9;mon Yellow: Special Pikachu Edition as well. Have the Pok&#xE9;mon that worked so hard for you in Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition keep battling by your side in Pok&#xE9;mon Gold and Pok&#xE9;mon Silver.

			These titles have also been slated to be compatible with Pok&#xE9;mon Bank, just as the Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow: Special Pikachu Edition are. Through Pok&#xE9;mon Bank, you can bring the Pok&#xE9;mon that you catch in the Virtual Console editions of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver to Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon as well, which were announced today to be released on November 17, 2017. Look forward to future releases with more updates about when this compatibility can be expected and other details.

			One of the first mainstream RPG games to appear on the Game Boy Color, Pok&#xE9;mon Gold Version and Pok&#xE9;mon Silver Version continued to expand the Pok&#xE9;mon universe, adding 100 Pok&#xE9;mon, held items, and real-time events, thanks to an internal game clock.

			These major Pok&#xE9;mon releases take you on a journey through the Johto region, and for the first time, show your Pok&#xE9;mon appearing in glorious color on your Game Boy Color. All kinds of creative aspects are to be found in Pok&#xE9;mon Gold and Pok&#xE9;mon Silver, including genders and items that Pok&#xE9;mon can hold to improve their skills.

			Dozens of Pok&#xE9;mon join the action, including the trio of Legendary Pok&#xE9;mon Raikou, Entei, and Suicune. They&#x27;re always on the move, so you&#x27;ll need to be at the right place at the right time to catch any of them. Pok&#xE9;mon Gold and Pok&#xE9;mon Silver introduce new Pok&#xE9;mon types as well&#x2012;look out for Steel- and Dark-type Pok&#xE9;mon throughout your travels.

			Pok&#xE9;mon Egg groups expand the ways you can collect Pok&#xE9;mon. Leave two compatible Pok&#xE9;mon at the Pok&#xE9;mon Day Care and cross your fingers. If you&#x27;re lucky, an Egg may appear that will hatch into a new young Pok&#xE9;mon! Only some Pok&#xE9;mon will give you an Egg, and Pok&#xE9;mon are picky about which other Pok&#xE9;mon they get along with, so it will take plenty of experimenting to find a match!

			Check out Pok&#xE9;mon Gold and Pok&#xE9;mon Silver on the Virtual Console starting September 22, 2017.', N'https://p9vruq.dm.files.1drv.com/y4me8BkxY7B7yWoJW5Td0nNaX4g98Kvg0cr1aTfcOI67dtZO06oZWY1DfXyFynMsolKa1-Qs-Ye7SVYGNLH6_Cwpag4hEIxps5_3wGey_F6ZHeu0vpztZAcsXQG35sstHEdrxeH4nQDtsJyTMnDovjAk0yE1NOq0pqwddK38xY7U8SltupqjWEqO8UFjpS5JbPf0Kba2PQw0MozEHm_OTaTMw?width=287&height=289&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (7, N'Pok&#xE9;mon Crystal', 2, N'Johto', N'2001-07-29', N'Game Boy Color', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-crystal-version/', N'The last of the Game Boy Color Pok&#xE9;mon games, Pok&#xE9;mon Crystal Version sent the classic system off in style.

			On the surface, Pok&#xE9;mon Crystal Version is a lot like Pok&#xE9;mon Gold Version and Pok&#xE9;mon Silver Version, in that the story takes place in Johto and the adventure takes you down a familiar path. But, digging deeper, you&#x27;ll see that Pok&#xE9;mon Crystal has plenty of aspects that have revolutionized Pok&#xE9;mon games. Pok&#xE9;mon Crystal is the first Pok&#xE9;mon game where you can choose to play either as a boy or girl, and it&#x27;s also the first Pok&#xE9;mon game to feature a Battle Tower, a central location for hordes of the toughest Trainers in the land!

			Pok&#xE9;mon Crystal features animated introductions for each Pok&#xE9;mon when they are brought to the heat of battle, as well as other graphic improvements you&#x27;ll notice as you roam the Johto region. While doing so, keep an eye out for Pok&#xE9;mon in different locations and Legendary Pok&#xE9;mon that can be caught only by the most talented Trainers!
			VIRTUAL CONSOLE:
			Relive a classic Pok&#xE9;mon adventure as Pok&#xE9;mon Crystal debuts on Nintendo eShop for the Nintendo 3DS family of systems. This is the definitive edition of the second generation of Pok&#xE9;mon titles (which also includes Pok&#xE9;mon Gold and Pok&#xE9;mon Silver), and fans will now be able to experience this journey through the Johto region with modern enhancements that make the game even better!

			Pok&#xE9;mon devotees will certainly want to check out this game to experience the first appearance of innovations that continue to be a part of the series to this day, such as a female hero and a Battle Tower. This generation, Pok&#xE9;mon Gold, Pok&#xE9;mon Silver, and Pok&#xE9;mon Crystal, also marks the debut of Dark- and Steel-type Pok&#xE9;mon, day/night cycles, and Pok&#xE9;mon Eggs.

			As you&#x27;re exploring, be sure to take advantage of updated features like wireless Pok&#xE9;mon battling and trading. Compatibility with Pok&#xE9; Transporter and Pok&#xE9;mon Bank will be added soon, so it will be easy to get any Legendary Pok&#xE9;mon you catch in Pok&#xE9;mon Crystal into your copies of Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon.', N'https://qnvruq.dm.files.1drv.com/y4m53qLLEVb0Azf9XW2XogR2Gst96cmwVOmL2uwdvUJVdTMtQwx65TD9YKBVZA30IogG8NEXEXRSCQfvk0C7zdlIZvYhhght26OSRsq-mLGhbaABCaGyfO7dpq8A7vZiFuqg30iNdMHKAEXdpxlvM9JYs1qZfkwRorhdk-gK9Llwxv2UCM8PH8C6v6gjkhfemOUhuFuFnAEwS-1a9kzeuTdSA?width=256&height=256&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (8, N'Pok&#xE9;mon Ruby', 3, N'Hoenn', N'2003-03-19', N'Game Boy Advanced', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-ruby-version-and-pokemon-sapphire-version/', N'The first Pok&#xE9;mon RPGs for the Game Boy Advance showed off the system&#x27;s improved graphics and sound, and introduced now-permanent fixtures to the Pok&#xE9;mon world, such as double battles and Contests.

			Pok&#xE9;mon reinvents itself once again with two of the most popular games in the series! Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version take great leaps in gameplay, particularly during combat. For the first time in a Pok&#xE9;mon game, Trainers can send two Pok&#xE9;mon into battle at the same time. Some Pok&#xE9;mon can learn moves that aid a partner or damage both of the opponent&#x27;s Pok&#xE9;mon. And Pok&#xE9;mon have Abilities and Natures that affect their performance in battle.

			Your Pok&#xE9;mon are more than just good fighters, however. Enter them in Contests where they can perform special moves in front of judges to capture the judges&#x27; attention. To improve their stage appeal, feed the Pok&#xE9;mon Pok&#xE9; Blocks made from Berries you cultivate around the region.

			Explore the world of the Hoenn region with a whole host of Pok&#xE9;mon. To make room for all of them, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire can hook up to Pok&#xE9;mon Box&#x2122;, a storage system on the Nintendo GameCube. A true test of Pok&#xE9;mon skills and strategies, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire definitely bring the level of Pok&#xE9;mon gameplay up a notch.', N'https://sdvruq.dm.files.1drv.com/y4mzjDOs-b5vsJR77I5-obRQrAuPL3ZpGu7cBrOy7Qk4g8yrpGP2dTQKlE9oLI6dBs_aPsRyLMil-PsVRZJ2LMNh74l8SxVYAyvwgiEWxljLnsEzIoGOQAP4Wn0oxdHFrOvONnJLg3inTsFbIAVEsaMgJFYZXjghg-We6L9R-5Kmw1Zm2frB4u0_hG8xG0g-5vw8GT46EBNdVfHj9VpJDKBCQ?width=500&height=500&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (9, N'Pok&#xE9;mon Sapphire', 3, N'Hoenn', N'2003-03-19', N'Game Boy Advanced', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-ruby-version-and-pokemon-sapphire-version/', N'The first Pok&#xE9;mon RPGs for the Game Boy Advance showed off the system&#x27;s improved graphics and sound, and introduced now-permanent fixtures to the Pok&#xE9;mon world, such as double battles and Contests.

			Pok&#xE9;mon reinvents itself once again with two of the most popular games in the series! Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version take great leaps in gameplay, particularly during combat. For the first time in a Pok&#xE9;mon game, Trainers can send two Pok&#xE9;mon into battle at the same time. Some Pok&#xE9;mon can learn moves that aid a partner or damage both of the opponent&#x27;s Pok&#xE9;mon. And Pok&#xE9;mon have Abilities and Natures that affect their performance in battle.

			Your Pok&#xE9;mon are more than just good fighters, however. Enter them in Contests where they can perform special moves in front of judges to capture the judges&#x27; attention. To improve their stage appeal, feed the Pok&#xE9;mon Pok&#xE9; Blocks made from Berries you cultivate around the region.

			Explore the world of the Hoenn region with a whole host of Pok&#xE9;mon. To make room for all of them, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire can hook up to Pok&#xE9;mon Box&#x2122;, a storage system on the Nintendo GameCube. A true test of Pok&#xE9;mon skills and strategies, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire definitely bring the level of Pok&#xE9;mon gameplay up a notch.', N'https://stvruq.dm.files.1drv.com/y4mg4Y8YWLSP_bCnIuaoq1EB76noySISpuQc-vYKQr9-1VBtGsROZEQ1BAgAOVaH6naJRB3R6j3lKBJPXDOMVuxvHfnxHDF0ArYYpbDDNwf3hdZrn1PsBm4XiLgxpX1HNTRVHySnCUHbPGCFpJXs-fXYSfhqXexdWRpYYzknML9_iX-jfHsyJxw_CaKrYH00Rng48DAyJArqaJb6Kc9MXVZ1Q?width=500&height=500&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (10, N'Pok&#xE9;mon Emerald', 3, N'Hoenn', N'2005-05-01', N'Game Boy Advanced', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-emerald-version/', N'Despite the Nintendo DS system making a splash, Pok&#xE9;mon Emerald Version showed that the Game Boy Advance was still an excellent system. Fortunately for Nintendo DS owners, you could transfer Pok&#xE9;mon from any of the Game Boy Advance games into future DS Pok&#xE9;mon RPGs.

			Pok&#xE9;mon Emerald Version takes Trainers back to the land of Hoenn for an expanded adventure, this time against both Team Magma and Team Aqua! Pok&#xE9;mon Emerald also features an even more exciting storyline featuring the Legendary Rayquaza, and the chance to catch more Legendary Pok&#xE9;mon such as both Latios and Latias!

			Around the region you&#x27;ll notice exciting locales, especially the Battle Frontier. The Battle Frontier is basically an amusement park for Trainers, with a variety of challenges in a number of arenas headed by the always-intimidating Frontier Brains, some of the most formidable Trainers you&#x27;ve ever faced.', N'https://qtxeag.dm.files.1drv.com/y4mQj0aZWfzFi3ZPebqnO9zzMhMTOMrx9SN5uKAxyxe4bq4cAOFjCAyco4EADJCZDprt2IeHHZrYndSjLQ8UiNlrcxK7cmbagg30kYALdxMyKv2BYIIKAAh79EWNe13VvfW0eVSZ8N8cuvvDv2nJf-4OSqNLIP5ksxES2a18sf5a9xECT9K3i2lilex3MXdBCqTWbcjfaScGj-WZiQKuDUWyg?width=1473&height=1475&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (11, N'Pok&#xE9;mon FireRed', 3, N'Kanto (Remake)', N'2004-09-09', N'Game Boy Advanced', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-firered-version-and-pokemon-leafgreen-version/', N'Six years after the original Pok&#xE9;mon Red Version and Pok&#xE9;mon Blue Version, this pair of titles for the Game Boy Advance system introduced Pok&#xE9;mon to a new group of gamers. These titles also shipped with a wireless adapter for Game Boy Advance, making it the first handheld to have such technology.

			Get set to return to where it all started&#x2012;Pok&#xE9;mon FireRed Version and Pok&#xE9;mon LeafGreen Version head back to the Kanto region, the home of Pok&#xE9;mon Red and Pok&#xE9;mon Blue. Thanks to the capabilities of the Game Boy Advance system, the locales, Pok&#xE9;mon, and animations are greatly improved since the last installment.

			Pok&#xE9;mon FireRed and Pok&#xE9;mon LeafGreen finally unite all the worlds of Pok&#xE9;mon, making it possible to catch and keep every single Pok&#xE9;mon in one game! You&#x27;ll need a little help from Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version, as well as Pok&#xE9;mon Stadium, but if it were too easy it wouldn&#x27;t be fun! To aid in trading Pok&#xE9;mon, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire come with a device for your Game Boy Advance that lets you trade Pok&#xE9;mon wirelessly. You can also chat or battle without the need for Game Link cables, too!

			Veterans of Pok&#xE9;mon Red and Pok&#xE9;mon Blue will appreciate that all the improvements made since the first visit to the Kanto region are available. Plus, there are different places to explore, most notably the Sevii Islands, where you can catch special Pok&#xE9;mon that don&#x27;t exist anywhere else in Kanto!', N'https://rnxeag.dm.files.1drv.com/y4ma1hw1YtjQCRu_LMrq9eZLqruzMee5Q-I4RlwbvgM2tkrsHCrKmbW81DoSNrgEoHPjq0KSPmAOmTeVAv78vSR2mBluclCwQKrDJ2n5i3Z-Khdt9Dfh8jGf0rXxedvTC0PrG6HhHQU1LMGPXdGf1OtVPnbGIYUp7lBkRneR1772Hv7ePMrOOa3ueR7GYJDu6xdmrIXp5UYdbeiatQyGVOK4w?width=400&height=400&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (12, N'Pok&#xE9;mon LeafGreen', 3, N'Kanto (Remake)', N'2004-09-09', N'Game Boy Advanced', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-firered-version-and-pokemon-leafgreen-version/', N'Six years after the original Pok&#xE9;mon Red Version and Pok&#xE9;mon Blue Version, this pair of titles for the Game Boy Advance system introduced Pok&#xE9;mon to a new group of gamers. These titles also shipped with a wireless adapter for Game Boy Advance, making it the first handheld to have such technology.

			Get set to return to where it all started&#x2012;Pok&#xE9;mon FireRed Version and Pok&#xE9;mon LeafGreen Version head back to the Kanto region, the home of Pok&#xE9;mon Red and Pok&#xE9;mon Blue. Thanks to the capabilities of the Game Boy Advance system, the locales, Pok&#xE9;mon, and animations are greatly improved since the last installment.

			Pok&#xE9;mon FireRed and Pok&#xE9;mon LeafGreen finally unite all the worlds of Pok&#xE9;mon, making it possible to catch and keep every single Pok&#xE9;mon in one game! You&#x27;ll need a little help from Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version, as well as Pok&#xE9;mon Stadium, but if it were too easy it wouldn&#x27;t be fun! To aid in trading Pok&#xE9;mon, Pok&#xE9;mon Ruby and Pok&#xE9;mon Sapphire come with a device for your Game Boy Advance that lets you trade Pok&#xE9;mon wirelessly. You can also chat or battle without the need for Game Link cables, too!

			Veterans of Pok&#xE9;mon Red and Pok&#xE9;mon Blue will appreciate that all the improvements made since the first visit to the Kanto region are available. Plus, there are different places to explore, most notably the Sevii Islands, where you can catch special Pok&#xE9;mon that don&#x27;t exist anywhere else in Kanto!', N'https://qdxeag.dm.files.1drv.com/y4mQzfx0sgYrV0D6wALnmY1pSYM-_eoNiLiYqWxx-ifCoJDXtLRgS3WoeUczU3HBVwpSTv8zjHDOF_KBf4eKxDBGAY-JL_VhHt9V6_RryTQ_HIQcG1x15l5Q886M6KbzPaYXr994qM2mlDg84T-yYKxRvMJk7GLkA-53je4dy0VcJ_vKEos3VpZUlZvdPRLoY9UjkwssXiJgmKy2mwR6fH2rg?width=800&height=803&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (13, N'Pok&#xE9;mon Diamond', 4, N'Sinnoh', N'2007-04-22', N'Nintendo DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-diamond-version-and-pokemon-pearl-version/', N'Pok&#xE9;mon Diamond Version and Pok&#xE9;mon Pearl Version introduce Trainers to a different land and many Pok&#xE9;mon to catch! Explore the lakes, forests, and mountains of the Sinnoh region, seeking out Pok&#xE9;mon such as the Bug-type Kricketot or the hard-headed Rock- and Steel-type Shieldon. And if you&#x27;re lucky, you might bump into Legendary Pok&#xE9;mon Palkia or Dialga! There are more than 490 Pok&#xE9;mon in Pok&#xE9;mon Diamond and Pok&#xE9;mon Pearl. But be on the lookout for Team Galactic, a dastardly group that is trying to kidnap Pok&#xE9;mon.

			Pok&#xE9;mon RPGs have never looked better than they do in Pok&#xE9;mon Diamond and Pok&#xE9;mon Pearl. 3-D graphics bring the land of Sinnoh to life! And realistic lighting casts the land in sun or darkness depending on the time of day. However, there&#x27;s more to daylight than just graphics&#x2012;some Pok&#xE9;mon will only show their faces during certain times of day, so you&#x27;ll have to explore morning, noon, and night if you want collect them all!

			Everywhere you look, gameplay has been revamped thanks to the awesome abilities of the Nintendo DS system. With two screens, you can watch the action on one screen while monitoring the health of your Pok&#xE9;mon on the other. The touch screen is used in a number of ways: You can use it to select moves in battle, to perform in Contests, or while exploring underground. And with the cool Trainer wristwatch, the Pok&#xE9;tch, you can keep tabs on all kinds of important data during your adventure.

			The Underground is formed by a series of subterranean passages below Sinnoh that hold untold numbers of treasures for you to find. But there&#x27;s more to the Underground than just spelunking&#x2012;you can meet up with other Trainers while you&#x27;re down there, and you can create a Secret Base for people to visit!

			As of May 20, 2014, certain online functionality offered through Nintendo Wi-Fi Connection will no longer be accessible, including online features of Pok&#xE9;mon Black Version, Pok&#xE9;mon White Version, Pok&#xE9;mon Black Version 2, and Pok&#xE9;mon White Version 2. You will still be able to enjoy the games in offline and local wireless modes.

			This functionality affects only Wii and Nintendo DS games&#x2012;online play for Wii U and Nintendo 3DS games such as Pok&#xE9;mon X and Pok&#xE9;mon Y will be unaffected.

			For more details, please visit http://support.nintendo.com/servicesupdate.', N'https://ptxeag.dm.files.1drv.com/y4m6fCA_m2Im43GcPxX-G_Ph9Qg7rEPvAhaIbbIjX9hjKxJN2_s89Q5DlPoAvO57JlDzU5CP2qkFRkzkzTBK-667NKRuEp0F96LfM6t08TaM0NwZXq8UZRCHruIwhrtWD-WFHytPCVrkV6AdDGVfDk6eLgHFa7E5fnwuSiNtKgqLJPU97MGxhZcy8QZ60EAEFGkIclbUDAebBQmXZ-33Z6Gww?width=1500&height=1344&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (14, N'Pok&#xE9;mon Pearl', 4, N'Sinnoh', N'2007-04-22', N'Nintendo DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-diamond-version-and-pokemon-pearl-version/', N'Pok&#xE9;mon Diamond Version and Pok&#xE9;mon Pearl Version introduce Trainers to a different land and many Pok&#xE9;mon to catch! Explore the lakes, forests, and mountains of the Sinnoh region, seeking out Pok&#xE9;mon such as the Bug-type Kricketot or the hard-headed Rock- and Steel-type Shieldon. And if you&#x27;re lucky, you might bump into Legendary Pok&#xE9;mon Palkia or Dialga! There are more than 490 Pok&#xE9;mon in Pok&#xE9;mon Diamond and Pok&#xE9;mon Pearl. But be on the lookout for Team Galactic, a dastardly group that is trying to kidnap Pok&#xE9;mon.

			Pok&#xE9;mon RPGs have never looked better than they do in Pok&#xE9;mon Diamond and Pok&#xE9;mon Pearl. 3-D graphics bring the land of Sinnoh to life! And realistic lighting casts the land in sun or darkness depending on the time of day. However, there&#x27;s more to daylight than just graphics&#x2012;some Pok&#xE9;mon will only show their faces during certain times of day, so you&#x27;ll have to explore morning, noon, and night if you want collect them all!

			Everywhere you look, gameplay has been revamped thanks to the awesome abilities of the Nintendo DS system. With two screens, you can watch the action on one screen while monitoring the health of your Pok&#xE9;mon on the other. The touch screen is used in a number of ways: You can use it to select moves in battle, to perform in Contests, or while exploring underground. And with the cool Trainer wristwatch, the Pok&#xE9;tch, you can keep tabs on all kinds of important data during your adventure.

			The Underground is formed by a series of subterranean passages below Sinnoh that hold untold numbers of treasures for you to find. But there&#x27;s more to the Underground than just spelunking&#x2012;you can meet up with other Trainers while you&#x27;re down there, and you can create a Secret Base for people to visit!

			As of May 20, 2014, certain online functionality offered through Nintendo Wi-Fi Connection will no longer be accessible, including online features of Pok&#xE9;mon Black Version, Pok&#xE9;mon White Version, Pok&#xE9;mon Black Version 2, and Pok&#xE9;mon White Version 2. You will still be able to enjoy the games in offline and local wireless modes.

			This functionality affects only Wii and Nintendo DS games&#x2012;online play for Wii U and Nintendo 3DS games such as Pok&#xE9;mon X and Pok&#xE9;mon Y will be unaffected.

			For more details, please visit http://support.nintendo.com/servicesupdate.', N'https://q9xeag.dm.files.1drv.com/y4mf_z2cZZyIJEhhp6vK_jZccN_ZOXEYeMybBDqhvzD6NGqidANOOApNL5_LD8-scuhHtO19-1A6mQKssnZEctDIP0fU6cHn_QGsFyWR_cRbEVV3Un8mBc2eHIGxlvjgBI56E9qMzdD5xF0tyS9CBwRW0w_RDQHqA4d5SOnoNTEG7BKzbc9Xf6Qzx-Xp7Omlhkr580mQj3EbXpvyXL4OoCyBQ?width=1531&height=1372&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (15, N'Pok&#xE9;mon Platinum', 4, N'Sinnoh', N'2009-03-22', N'Nintendo DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-platinum-version/', N'Another world has emerged in the Sinnoh region, a place where time and space have been completely altered. Explore the Distortion World, where challenges await at every turn! Visit other locations designed especially for the serious Trainer: A Battle Frontier will test your battle skills with its abundance of committed opponents, led by the Frontier Brains. Also check out the Wi-Fi Plaza, where up to 20 players can connect with a wireless broadband internet connection and participate in fun games and activities.

			While Dialga and Palkia shared the limelight in Pok&#xE9;mon Diamond Version and Pok&#xE9;mon Pearl Version, the Legendary Pok&#xE9;mon Giratina is at the center of the story in Pok&#xE9;mon Platinum Version. As you track down the elusive Giratina, watch out for Team Galactic&#x2012;they&#x27;re back, and with Cyrus leading, they&#x27;re as dangerous as ever.

			Check out the expanded Global Trade Station, now known as the Global Terminal. Various upgrades include the ability to record your battles using the Vs. Recorder. Once the battles have been recorded, post them for the world to see how awesome your team really is!

			As of May 20, 2014, certain online functionality offered through Nintendo Wi-Fi Connection will no longer be accessible, including online features of Pok&#xE9;mon Black Version, Pok&#xE9;mon White Version, Pok&#xE9;mon Black Version 2, and Pok&#xE9;mon White Version 2. You will still be able to enjoy the games in offline and local wireless modes.

			This functionality affects only Wii and Nintendo DS games&#x2012;online play for Wii U and Nintendo 3DS games such as Pok&#xE9;mon X and Pok&#xE9;mon Y will be unaffected.

			For more details, please visit http://support.nintendo.com/servicesupdate.', N'https://pdxeag.dm.files.1drv.com/y4mRU54gp7u7RFZnZmuTO-KV6aVrEPQz8qqX7UHqYh80Cocg0aAQ6BokLMKKcSXXkOtZpAptkXE_vbmkV8wmx-sofjdXO8V6YAvALVpurGH_k_SOCeK5pjtNHHrXBAmj7xpaxKKllvexKcw6ShWs8avgvdKhWlRiRlvbwmp_MAsKMLbfDLGvtzx-EPSSNQqz3rwh43i1XH52wPtIbnWO3chZQ?width=476&height=434&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (16, N'Pok&#xE9;mon HeartGold', 4, N'Johto (Remake)', N'2010-03-10', N'Nintendo DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-heartgold-and-soulsilver-versions/', N'Pok&#xE9;mon HeartGold and SoulSilver Versions are available for the Nintendo DS and Nintendo DSi systems today!

			It&#x27;s been nearly a decade since Pok&#xE9;mon fans first traveled to the scenic Johto region. With Pok&#xE9;mon HeartGold and SoulSilver Versions, they can return there to experience the exciting adventure of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver on the Nintendo DS and Nintendo DSi systems. Check out updated graphics and sound, as well as awesome new touch-screen features and lots of surprises! Pok&#xE9;mon HeartGold and SoulSilver Versions bring dozens of Pok&#xE9;mon characters, such as starters Chikorita, Cyndaquil, and Totodile, back into the limelight for a new group of Pok&#xE9;mon fans&#x2012;and long-time Trainers&#x2012;to catch, train and battle!

			More to Touch!

			Plenty of enhancements have been to the gameplay, including all-new touch-screen controls! You can navigate your Pok&#xE9;dex, check out the inventory in your Bag, and even move Pok&#xE9;mon around inside your PC Boxes. If you&#x27;re a Trainer who would rather use the buttons, don&#x27;t worry&#x2012;both methods will be available, so you can play in whatever way suits you best!

			The Pok&#xE9;walker!

			Pok&#xE9;mon HeartGold and SoulSilver Versions come with the brand-new Pok&#xE9;walker, an accesory that brings a dimension to Pok&#xE9;mon games that you&#x27;ve never seen before. The Pok&#xE9;walker is a special pedometer that you carry with you that lets you take Pok&#xE9;mon fun wherever you go! You can transfer a Pok&#xE9;mon onto the Pok&#xE9;walker accessory using an infrared connection between the Pok&#xE9;walker and your Game Card. Then as you Stroll around with your Pok&#xE9;mon, it&#x27;ll earn Experience Points. And you can even find items and new Pok&#xE9;mon while you&#x27;re on the go! The Pok&#xE9;walker is small enough that you can keep it in your pocket and take wherever you go. It also has a clip to hook it on your belt, and a hole for attaching a strap. It&#x27;s never been easier to take Pok&#xE9;mon with you!', N'https://stxeag.dm.files.1drv.com/y4m6Yb4KkjKk44lM8c9mvRzZmmZFz1b4Q4POgnthDEiDvHb9LT5pPwEhSFvkIme__5TNXhqJYaGZkTZ1S8IeZEhitEfWUXHxLnHzal_SzeLSaJYfI3YFb053tXV1sGo3KuJyzHSXYcrOohvDc-N-1jWAb_zvRxWpEglbAvk5OtsKJvYTTkJkmKNYJvksAKntfPCQtg9WT9k5DG-i2dNthtCUg?width=1000&height=912&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (17, N'Pok&#xE9;mon SoulSilver', 4, N'Johto (Remake)', N'2010-03-10', N'Nintendo DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-heartgold-and-soulsilver-versions/', N'Pok&#xE9;mon HeartGold and SoulSilver Versions are available for the Nintendo DS and Nintendo DSi systems today!

			It&#x27;s been nearly a decade since Pok&#xE9;mon fans first traveled to the scenic Johto region. With Pok&#xE9;mon HeartGold and SoulSilver Versions, they can return there to experience the exciting adventure of Pok&#xE9;mon Gold and Pok&#xE9;mon Silver on the Nintendo DS and Nintendo DSi systems. Check out updated graphics and sound, as well as awesome new touch-screen features and lots of surprises! Pok&#xE9;mon HeartGold and SoulSilver Versions bring dozens of Pok&#xE9;mon characters, such as starters Chikorita, Cyndaquil, and Totodile, back into the limelight for a new group of Pok&#xE9;mon fans&#x2012;and long-time Trainers&#x2012;to catch, train and battle!

			More to Touch!

			Plenty of enhancements have been to the gameplay, including all-new touch-screen controls! You can navigate your Pok&#xE9;dex, check out the inventory in your Bag, and even move Pok&#xE9;mon around inside your PC Boxes. If you&#x27;re a Trainer who would rather use the buttons, don&#x27;t worry&#x2012;both methods will be available, so you can play in whatever way suits you best!

			The Pok&#xE9;walker!

			Pok&#xE9;mon HeartGold and SoulSilver Versions come with the brand-new Pok&#xE9;walker, an accesory that brings a dimension to Pok&#xE9;mon games that you&#x27;ve never seen before. The Pok&#xE9;walker is a special pedometer that you carry with you that lets you take Pok&#xE9;mon fun wherever you go! You can transfer a Pok&#xE9;mon onto the Pok&#xE9;walker accessory using an infrared connection between the Pok&#xE9;walker and your Game Card. Then as you Stroll around with your Pok&#xE9;mon, it&#x27;ll earn Experience Points. And you can even find items and new Pok&#xE9;mon while you&#x27;re on the go! The Pok&#xE9;walker is small enough that you can keep it in your pocket and take wherever you go. It also has a clip to hook it on your belt, and a hole for attaching a strap. It&#x27;s never been easier to take Pok&#xE9;mon with you!', N'https://qnxeag.dm.files.1drv.com/y4m2OgbZ99N3A5l6ym6yTQGo6Jiit0h4dXQro1_XkwaIvGTLRzE6gZsFMBHPiDEFtfvkpymGluDjdfVJ6vOfwtPCV1KqIbnbyLlYbmH9ttc5yFa5Wl680jr3ZYw1wKl7F2rTY_ef-QXi3rvYUh41fUs72fH7MCheZwqzSHGTfdcbi0Plu6te4JDqD7avKsjICImmpIO_LeIWMwJhVvH0t6jeQ?width=220&height=201&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (18, N'Pok&#xE9;mon Black', 5, N'Unova', N'2011-03-06', N'NIntendo DSi', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-black-version-and-pokemon-white-version/', N'Pok&#xE9;mon&#x2122; Black Version and Pok&#xE9;mon White Version are now available on the Nintendo DS&#x2122; family of systems! Travel through the Unova region to discover new Pok&#xE9;mon, tough trainers, and many exciting adventures!

			Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version features a new generation of amazing Pok&#xE9;mon. The Legendary Pok&#xE9;mon Reshiram appears on the cover of Pok&#xE9;mon Black Version and the Legendary Pok&#xE9;mon Zekrom is seen on the cover of Pok&#xE9;mon White Version. You will begin your adventure with your choice of Snivy, Tepig, or Oshawott&#x2012;Pok&#xE9;mon who are eager to join you on your epic journey!', N'https://p9xeag.dm.files.1drv.com/y4mVOsF8PMXlZgmt-7uPFsmBfBn2HJVOBbhCy1F8iQquygaj3Gg1IX7p9bVsThrGOacTESk8Mq7pDlc_XKcFRdxi4MUUWHI5pLP_i5ykjWq_h3TzJi6oYmOJpJ-XWv1LibYi09upa1FRmYBv6wJH-dQeZ32EUhs7oXs5jHBMntcQW-bbgmdOPuXSLg0IMFIauJjI-VP_5zZeeWBsnNQaEhthQ?width=1000&height=897&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (19, N'Pok&#xE9;mon White', 5, N'Unova', N'2011-03-06', N'Nintendo DSi', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-black-version-and-pokemon-white-version/', N'Pok&#xE9;mon&#x2122; Black Version and Pok&#xE9;mon White Version are now available on the Nintendo DS&#x2122; family of systems! Travel through the Unova region to discover new Pok&#xE9;mon, tough trainers, and many exciting adventures!

			Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version features a new generation of amazing Pok&#xE9;mon. The Legendary Pok&#xE9;mon Reshiram appears on the cover of Pok&#xE9;mon Black Version and the Legendary Pok&#xE9;mon Zekrom is seen on the cover of Pok&#xE9;mon White Version. You will begin your adventure with your choice of Snivy, Tepig, or Oshawott&#x2012;Pok&#xE9;mon who are eager to join you on your epic journey!', N'https://sdxeag.dm.files.1drv.com/y4m_MdJ53yrtPsJfVJPYvD6BwLKJl1voVSHrjXaqcGjy8mLjzpSQHt-Rgum91AU9nqqcHC46zRPlAkCNAFrJyCyfbCDYFg0yPvKrUl2Mne5ulpqdiWmD_iLW5KXgqvwXf-mqt8BK1P6GFgT3HkZ7BdsYGM2dChBGhm6do2BXyCI8XKcNynjxF9Hf2a3ywSEQYjT2zPpJ0un4FopgfO55yX-4A?width=1000&height=896&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (20, N'Pok&#xE9;mon Black 2', 5, N'Unova (Revisited)', N'2012-10-07', N'Nintendo DSi', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-black-version-2-and-pokemon-white-version-2/', N'Pok&#xE9;mon fans, get excited for two incredible new adventures! Pok&#xE9;mon Black Version 2 and Pok&#xE9;mon White Version 2 are available now for the Nintendo DS&#x2122; family of systems. The games can also be played in 2D on the Nintendo 3DS&#x2122; system.

			Your journey takes place in the Unova region two years after the events of Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version. You&#x27;ll start your adventure in Aspertia City, in the far southwest corner of the Unova region. Many things have changed in the region, including some extraordinary new places and people for you to discover. Plus, many of the returning characters from Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version have taken on new roles when you meet them.

			Look forward to uncovering the mystery of Black Kyurem&#x2012;or White Kyurem&#x2012;somewhere in the Unova region. Little is known about this enigmatic Pok&#xE9;mon, except that it&#x27;s a Dragon- and Ice-type Pok&#xE9;mon with a devastating Ice-type attack&#x2012;even stronger than Zekrom&#x27;s Fusion Bolt or Reshiram&#x27;s Fusion Flare!

			Pok&#xE9;mon Black Version 2 and Pok&#xE9;mon White Version 2 have a special connection with Pok&#xE9;mon Dream Radar for the Nintendo 3DS system. The Pok&#xE9;mon you obtain in Pok&#xE9;mon Dream Radar can be sent to your Pok&#xE9;mon Black Version 2 or Pok&#xE9;mon White Version 2 game! Get more information on Pok&#xE9;mon Dream Radar now.

			And to help you with the many Pok&#xE9;mon found in the Unova region, look forward to Pok&#xE9;dex 3D Pro, another Nintendo 3DS title that&#x27;s on the way to the Nintendo eShop. With Pok&#xE9;dex 3D Pro, you&#x27;ll have information on every Pok&#xE9;mon in the National Pok&#xE9;dex&#x2012;just what you need to become a master Trainer. Learn more about Pok&#xE9;dex 3D Pro!', N'https://qtvf3w.dm.files.1drv.com/y4mogPSWYb0_hD6jSN0Joaq9cY-_3EumUIN3oPeK_e4YTNBYvLCfQc5gibDb_PHZbsFSboxqVJEnWSsoqwdoaYUkqnBLOMe1rwhQsgEpRYCRG1cxTIGxKDW0IOV5WAaMWu19gHXjd0w06g-tEl6ziqrf7rbcHGMDuU1YD3ZKoJVpVNMpNVzqUdZpy_9PjqzwEo8CCJrz6GrNC2sV1kefkgtKA?width=656&height=600&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (21, N'Pok&#xE9;mon White 2', 5, N'Unova (Revisited)', N'2012-10-07', N'Nintendo DSi', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-black-version-2-and-pokemon-white-version-2/', N'Pok&#xE9;mon fans, get excited for two incredible new adventures! Pok&#xE9;mon Black Version 2 and Pok&#xE9;mon White Version 2 are available now for the Nintendo DS&#x2122; family of systems. The games can also be played in 2D on the Nintendo 3DS&#x2122; system.

			Your journey takes place in the Unova region two years after the events of Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version. You&#x27;ll start your adventure in Aspertia City, in the far southwest corner of the Unova region. Many things have changed in the region, including some extraordinary new places and people for you to discover. Plus, many of the returning characters from Pok&#xE9;mon Black Version and Pok&#xE9;mon White Version have taken on new roles when you meet them.

			Look forward to uncovering the mystery of Black Kyurem&#x2012;or White Kyurem&#x2012;somewhere in the Unova region. Little is known about this enigmatic Pok&#xE9;mon, except that it&#x27;s a Dragon- and Ice-type Pok&#xE9;mon with a devastating Ice-type attack&#x2012;even stronger than Zekrom&#x27;s Fusion Bolt or Reshiram&#x27;s Fusion Flare!

			Pok&#xE9;mon Black Version 2 and Pok&#xE9;mon White Version 2 have a special connection with Pok&#xE9;mon Dream Radar for the Nintendo 3DS system. The Pok&#xE9;mon you obtain in Pok&#xE9;mon Dream Radar can be sent to your Pok&#xE9;mon Black Version 2 or Pok&#xE9;mon White Version 2 game! Get more information on Pok&#xE9;mon Dream Radar now.

			And to help you with the many Pok&#xE9;mon found in the Unova region, look forward to Pok&#xE9;dex 3D Pro, another Nintendo 3DS title that&#x27;s on the way to the Nintendo eShop. With Pok&#xE9;dex 3D Pro, you&#x27;ll have information on every Pok&#xE9;mon in the National Pok&#xE9;dex&#x2012;just what you need to become a master Trainer. Learn more about Pok&#xE9;dex 3D Pro!', N'https://qdvf3w.dm.files.1drv.com/y4m_kMZNEZuo7oo5BGee9PYoGcDsdc6jhcyWazGpkXoiRkUn5-vSj2kihKu2iON1FPADaw_SldGyD-kZ6x7WmpE1f2yUuOJgKxMPaBtbt5majFgtOexrYfdHk23cUM3kPkiedgLq8gheZmbXWaMchg77Zlu6Hc92zbsZGLxpmSe49kCwPrVvSTaE69xRkR-nXrzmGF3Gkp1QfpmVWV0HhUgGg?width=250&height=223&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (22, N'Pok&#xE9;mon X', 6, N'Kalos', N'2013-10-12', N'Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-x-and-pokemon-y/', N'An all-new 3D Pok&#xE9;mon adventure packed with never-before-seen Pok&#xE9;mon has launched! Pok&#xE9;mon X and Pok&#xE9;mon Y presents a new generation of Pok&#xE9;mon and introduces players to an exciting new adventure in a breathtaking 3D world.

			You&#x27;ll be able to go on your journey as a boy or a girl, and you can choose how your hero looks overall! Find a look that suits you, and then set off on your travels!

			Your adventure takes place in the expansive Kalos region, where you&#x27;ll explore cities, meet people, and encounter many Pok&#xE9;mon. Don&#x27;t miss Lumiose City, the central hub of the region, where you&#x27;ll return many times on your journey.

			Look for all-new Fairy-type Pok&#xE9;mon, the first new Pok&#xE9;mon type since Dark-type and Steel-type Pok&#xE9;mon were introduced almost a decade ago. Fairy-type moves are super effective against Dragon-type Pok&#xE9;mon. One such Fairy-type Pok&#xE9;mon to meet is Sylveon, the new evolved form of Eevee!

			All-new battle formats are fun for new players and Pok&#xE9;mon pros. Sky Battles feature duels between Pok&#xE9;mon that can fly, and Horde encounters are massive competitions where one of your Pok&#xE9;mon faces many wild Pok&#xE9;mon at once! These new modes will challenge every Pok&#xE9;mon Trainer.

			In addition to new battle types, you can also care for your Pok&#xE9;mon with the Pok&#xE9;mon-Amie feature. Feed, pet, and play with your Pok&#xE9;mon to increase your bond. The more you play with your Pok&#xE9;mon, the friendlier they&#x27;ll become&#x2012;and perhaps perform better for you in battle.

			Around the Kalos region, you&#x27;ll meet a lot of people. First there&#x27;s the brilliant Professor Sycamore. He&#x27;s researching Pok&#xE9;mon, and he&#x27;s looking to you and your friends for help. He may even challenge you to a battle from time to time. But you&#x27;re also likely to encounter members of the mysterious organization Team Flare. What they&#x27;re up to is anyone&#x27;s guess.

			In Pok&#xE9;mon X and Pok&#xE9;mon Y, some Pok&#xE9;mon will tap into powers long dormant. This special kind of Evolution is called Mega Evolution. In addition to far greater strength, Mega-Evolved Pok&#xE9;mon may also see their Ability change, or even their type!

			There&#x27;s a new method of Pok&#xE9;mon training: Super Training! With Super Training, it&#x27;s not your Pok&#xE9;mon&#x27;s level that goes up but its base stats, the underlying values that define your Pok&#xE9;mon&#x27;s skills in battle. These base stats could be increased using items and other methods in previous Pok&#xE9;mon titles, but in Pok&#xE9;mon X and Pok&#xE9;mon Y, you&#x27;ll be able to raise them far more easily than ever before, thanks to Super Training.

			The all-new Player Search System (PSS) gives you all kinds of new ways to interact with other players. With the PSS, you can battle and trade with players around the world, as well as share O-Powers, special abilities that will temporarily enhance certain gameplay aspects. And the Holo Caster feature of the PSS lets you trade messages with faraway Trainers and receive Pok&#xE9;mon X and Pok&#xE9;mon Y news bulletins.

			The Pok&#xE9;mon Global Link has been completely redesigned to be easier and more fun to use! Now, directly from the PGL homepage, see your game progress, enter Online Competitions, and more. You can even share a timeline of your adventures with your friends and the whole world!

			See the game in action at Pok&#xE9;mon.com/XY!', N'https://rnvf3w.dm.files.1drv.com/y4mxP3D4ldyk28xFKqiWy1UVAPBv8GVgdzQXjBq0TN4bhBkX6AC-8GKHlAn6fKknElQMkzQW1n8MotrvKjGxQPnJ89yq_qeu91gm46qYoCXaUlTcbLE0bNfcl05JWqu-F1fbx_E4wXrQQ0iyWUwY3K5Xpl8lJEIvAuzcoR413I6MQYcupuvH5I8hpURJMbqKOXm_dYKGjPcUhtKqBMmttawtQ?width=1200&height=1096&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (23, N'Pok&#xE9;mon Y', 6, N'Kalos', N'2013-10-12', N'Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-x-and-pokemon-y/', N'An all-new 3D Pok&#xE9;mon adventure packed with never-before-seen Pok&#xE9;mon has launched! Pok&#xE9;mon X and Pok&#xE9;mon Y presents a new generation of Pok&#xE9;mon and introduces players to an exciting new adventure in a breathtaking 3D world.

			You&#x27;ll be able to go on your journey as a boy or a girl, and you can choose how your hero looks overall! Find a look that suits you, and then set off on your travels!

			Your adventure takes place in the expansive Kalos region, where you&#x27;ll explore cities, meet people, and encounter many Pok&#xE9;mon. Don&#x27;t miss Lumiose City, the central hub of the region, where you&#x27;ll return many times on your journey.

			Look for all-new Fairy-type Pok&#xE9;mon, the first new Pok&#xE9;mon type since Dark-type and Steel-type Pok&#xE9;mon were introduced almost a decade ago. Fairy-type moves are super effective against Dragon-type Pok&#xE9;mon. One such Fairy-type Pok&#xE9;mon to meet is Sylveon, the new evolved form of Eevee!

			All-new battle formats are fun for new players and Pok&#xE9;mon pros. Sky Battles feature duels between Pok&#xE9;mon that can fly, and Horde encounters are massive competitions where one of your Pok&#xE9;mon faces many wild Pok&#xE9;mon at once! These new modes will challenge every Pok&#xE9;mon Trainer.

			In addition to new battle types, you can also care for your Pok&#xE9;mon with the Pok&#xE9;mon-Amie feature. Feed, pet, and play with your Pok&#xE9;mon to increase your bond. The more you play with your Pok&#xE9;mon, the friendlier they&#x27;ll become&#x2012;and perhaps perform better for you in battle.

			Around the Kalos region, you&#x27;ll meet a lot of people. First there&#x27;s the brilliant Professor Sycamore. He&#x27;s researching Pok&#xE9;mon, and he&#x27;s looking to you and your friends for help. He may even challenge you to a battle from time to time. But you&#x27;re also likely to encounter members of the mysterious organization Team Flare. What they&#x27;re up to is anyone&#x27;s guess.

			In Pok&#xE9;mon X and Pok&#xE9;mon Y, some Pok&#xE9;mon will tap into powers long dormant. This special kind of Evolution is called Mega Evolution. In addition to far greater strength, Mega-Evolved Pok&#xE9;mon may also see their Ability change, or even their type!

			There&#x27;s a new method of Pok&#xE9;mon training: Super Training! With Super Training, it&#x27;s not your Pok&#xE9;mon&#x27;s level that goes up but its base stats, the underlying values that define your Pok&#xE9;mon&#x27;s skills in battle. These base stats could be increased using items and other methods in previous Pok&#xE9;mon titles, but in Pok&#xE9;mon X and Pok&#xE9;mon Y, you&#x27;ll be able to raise them far more easily than ever before, thanks to Super Training.

			The all-new Player Search System (PSS) gives you all kinds of new ways to interact with other players. With the PSS, you can battle and trade with players around the world, as well as share O-Powers, special abilities that will temporarily enhance certain gameplay aspects. And the Holo Caster feature of the PSS lets you trade messages with faraway Trainers and receive Pok&#xE9;mon X and Pok&#xE9;mon Y news bulletins.

			The Pok&#xE9;mon Global Link has been completely redesigned to be easier and more fun to use! Now, directly from the PGL homepage, see your game progress, enter Online Competitions, and more. You can even share a timeline of your adventures with your friends and the whole world!

			See the game in action at Pok&#xE9;mon.com/XY!', N'https://q9vf3w.dm.files.1drv.com/y4mucSkbvP-Ic7G5sTt22nLjYiVY_-TGXunVYdoN5zlslZvQOuPvYu_PyzfRWPbf-4TYMGKJy0kh3RPlJuQoWB8gk4j-NVZWwvR3zDgXTcRCv8vJMRNouOhQp8-M2Moh0pt1XHZGdicPYhxOIyosz8VXkfwUeXmfLxzMcpf7l-brvdRcnqsIAiU3l7cVlW7hWD3ScyrnTGwjBWT5lWMY3UA2g?width=833&height=762&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (24, N'Pok&#xE9;mon Omega Ruby', 6, N'Hoenn (Remake)', N'2014-11-21', N'Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-omega-ruby-and-pokemon-alpha-sapphire/', N'Prepare for an epic adventure exploring a world filled with Pok&#xE9;mon in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire for the Nintendo 3DS family of systems.

			Your adventure takes place in Hoenn, a region that consists of a main island that stretches widely from east to west, along with countless small islets that dot the deep blue sea around it. A live volcano steams constantly in the heart of this green-covered island. Look forward to a region rich with natural beauty, a variety of Pok&#xE9;mon, and extraordinary people!

			The identities of the Pok&#xE9;mon on the boxes of Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire have been revealed! Their names are Primal Groudon and Primal Kyogre, respectively. These Pok&#xE9;mon will have a great role to play in shaping the stories in which they each appear!

			Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire tell a grand tale that draws ever closer to the heart of the secrets behind Mega Evolution, said to be the greatest mystery of the Pok&#xE9;mon world. One of your many goals will be to seek out these powerful Pok&#xE9;mon and unlock their incredible potential.

			During your journey through Hoenn, you&#x27;ll be able to build and invite friends to visit your personal Super-Secret Base! Come up with your own Secret Base design, including selecting and placing Decorations however you like. Then, using communication features, you&#x27;ll be able to easily connect with other players all around the world to share your Secret Bases with one another. When you visit another player&#x27;s Secret Base, that base&#x27;s members will be hanging out there. You can talk to them or even battle them! If you find other base owners that you like, you can scout them as one of your Secret Pals and bring them back to your own Secret Base.

			You can also use Super-Secret Bases to play a capture-the-flag activity. Examine the flag beside the Secret PC in another player&#x27;s Secret Base to obtain it! You can take one flag per day from each base visited. Visit lots of Secret Bases and collect lots of flags to improve your team&#x27;s rank and get rewards, including increasing the skills of your Secret Pals.

			Meet many interesting people and Pok&#xE9;mon Trainers as you travel across the Hoenn region, including Pok&#xE9;mon Professor Birch, who runs the Pok&#xE9;mon Lab in Littleroot Town. With him you will have your first meeting with the Pok&#xE9;mon that will become your partner on your journey, and a new bond will be born. You&#x27;ll also encounter incredibly tough Trainers during your journey. Visit Pok&#xE9;mon Gyms, where Pok&#xE9;mon Trainers gather and Gym Leaders stand ready for Trainers to challenge them.

			The Pok&#xE9;mon-Amie feature, which debuted in Pok&#xE9;mon X and Pok&#xE9;mon Y, enables players to get closer to their Pok&#xE9;mon by petting them, playing games with them, and feeding them. You can play with the Pok&#xE9;mon on your team at any time using Pok&#xE9;mon-Amie&#x2012;the more you care for your Pok&#xE9;mon in these ways, the deeper your bond will become. As your Pok&#xE9;mon become more affectionate toward you, they will be more likely to avoid attacks in battle, land critical hits on opponents, and perform more admirably than before.

			During your adventure, you&#x27;ll be able to create your very own Secret Base. Work on making a space that is all your own, just the way you like it. But that&#x27;s not even the best part! Using communication features, you&#x27;ll be able to easily connect with other players all around the world to share your Secret Bases with one another.

			You won&#x27;t raise your Pok&#xE9;mon just for battle in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire. In Pok&#xE9;mon Contest Spectaculars, your Pok&#xE9;mon will compete to see who has the most appeal! There are five kinds of contests that you can choose to enter: Coolness, Beauty, Cuteness, Cleverness, and Toughness. Your Pok&#xE9;mon will be judged on its appeal based on one of these five conditions. Choose a contest condition in which your Pok&#xE9;mon excels, and enter it in one of the corresponding contests!

			Enhance your Pok&#xE9;mon&#x27;s ability to perform by feeding it Pok&#xE9;blocks made from Berries. The type of Pok&#xE9;block you make is based on the color and variety of Berries you choose. When you feed them to your Pok&#xE9;mon, its contest conditions will improve according to the type of Pok&#xE9;block it eats!

			During your adventure, you will battle Team Magma in Pok&#xE9;mon Omega Ruby and Team Aqua in Pok&#xE9;mon Alpha Sapphire. Led by Maxie, Team Magma seeks to increase the land, while Archie and Team Aqua wish to increase the seas. In order to bring about these grand plans, each will turn to the power of a Legendary Pok&#xE9;mon: Groudon or Kyogre. Team Magma pursues Groudon, while Team Aqua goes after Kyogre. What could be driving them to such lengths?

			Look forward to encountering the Eon Pok&#xE9;mon Latios and Latias! In Pok&#xE9;mon Omega Ruby, you can team up with Latios, while Latias joins you in Pok&#xE9;mon Alpha Sapphire. And it has been discovered that Latios and Latias can also Mega Evolve, another exciting moment to anticipate!

			Partway through your adventure, you&#x27;ll become able to Soar in the sky. Gaze down over the wide Hoenn region as you Soar freely wherever you wish! Among the destinations you can reach using Soar are very special locations called Mirage spots. In Mirage spots, you may be able to find rare Pok&#xE9;mon that cannot be found elsewhere in the region, and you may be able to obtain valuable items.

			The Pok&#xE9;Nav Plus is your best friend on your journey. It&#x27;s a handy tool that will help you navigate your adventure as a Pok&#xE9;mon Trainer. It provides a lot of useful information, making your adventure easier and more fun than ever. Outside of battles and special events, the Pok&#xE9;Nav Plus will always be displayed on the lower screen of your Nintendo 3DS system. You can instantly switch between its four main functions with touch input, and be ready to play in no time.

			In Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire, a new story unfolds involving the Hoenn region&#x2012;the Delta Episode! This episode involves the history of the Hoenn region, reaching 3,000 years into the past, and makes the mysteries of Mega Evolution clear at long last. The troubled tale begins with Zinnia, a young woman who bears a certain destiny. The story includes appearances from Team Magma and Team Aqua, Steven, Gym Leader Wallace, and other familiar faces. Don&#x27;t miss the conflict between Rayquaza and Deoxys!

			Look for Mirage spots, where you may be able to find rare Pok&#xE9;mon that can&#x27;t be found elsewhere in the region and also obtain valuable items. Keep on the lookout for Legendary Pok&#xE9;mon that have recently been discovered at new Mirage spots. Note that the Legendary Pok&#xE9;mon you&#x27;ll encounter sometimes vary based on which game you&#x27;re playing. With Pok&#xE9;mon X, Pok&#xE9;mon Y, Pok&#xE9;mon Omega Ruby, and Pok&#xE9;mon Alpha Sapphire, every Legendary Pok&#xE9;mon discovered to date can be obtained!

			In Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire, you can obtain the Mega Stones needed to Mega Evolve all of the Mega-Evolving Pok&#xE9;mon that appeared in Pok&#xE9;mon X and Pok&#xE9;mon Y. Be sure to look high and low to find these mysterious and amazing items!

			Pok&#xE9;mon fans first enjoyed Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version when it launched in 2003 for the Game Boy Advance. Now, a whole new generation of Pok&#xE9;mon fans will experience the dramatic storyline that unfolds in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire.

			Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire are now available in stores and in Nintendo eShop.', N'https://ptvf3w.dm.files.1drv.com/y4mHzhSCuosgEQScNrQInlmqloQAbpc2l3Gq7PqzFMA7cn5GNwXiRxLpDSIXVOPl8VzVkKXqObQcK-So1eSkhxREmk_joAknp43kaW4SKzetGOgWDgYA5sWMovDaVj6MMaYucevgFhiFQ9aoUorCQVqKnQE03ZD95GELrCRLhkSu8Wwwbd-pBsp-ZQ5PhZ44JqtZvD32qXSb7xaqrY9PxnqTg?width=1798&height=1600&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (25, N'Pok&#xE9;mon Alpha Sapphire', 6, N'Hoenn (Remake)', N'2014-11-21', N'Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-omega-ruby-and-pokemon-alpha-sapphire/', N'Prepare for an epic adventure exploring a world filled with Pok&#xE9;mon in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire for the Nintendo 3DS family of systems.

			Your adventure takes place in Hoenn, a region that consists of a main island that stretches widely from east to west, along with countless small islets that dot the deep blue sea around it. A live volcano steams constantly in the heart of this green-covered island. Look forward to a region rich with natural beauty, a variety of Pok&#xE9;mon, and extraordinary people!

			The identities of the Pok&#xE9;mon on the boxes of Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire have been revealed! Their names are Primal Groudon and Primal Kyogre, respectively. These Pok&#xE9;mon will have a great role to play in shaping the stories in which they each appear!

			Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire tell a grand tale that draws ever closer to the heart of the secrets behind Mega Evolution, said to be the greatest mystery of the Pok&#xE9;mon world. One of your many goals will be to seek out these powerful Pok&#xE9;mon and unlock their incredible potential.

			During your journey through Hoenn, you&#x27;ll be able to build and invite friends to visit your personal Super-Secret Base! Come up with your own Secret Base design, including selecting and placing Decorations however you like. Then, using communication features, you&#x27;ll be able to easily connect with other players all around the world to share your Secret Bases with one another. When you visit another player&#x27;s Secret Base, that base&#x27;s members will be hanging out there. You can talk to them or even battle them! If you find other base owners that you like, you can scout them as one of your Secret Pals and bring them back to your own Secret Base.

			You can also use Super-Secret Bases to play a capture-the-flag activity. Examine the flag beside the Secret PC in another player&#x27;s Secret Base to obtain it! You can take one flag per day from each base visited. Visit lots of Secret Bases and collect lots of flags to improve your team&#x27;s rank and get rewards, including increasing the skills of your Secret Pals.

			Meet many interesting people and Pok&#xE9;mon Trainers as you travel across the Hoenn region, including Pok&#xE9;mon Professor Birch, who runs the Pok&#xE9;mon Lab in Littleroot Town. With him you will have your first meeting with the Pok&#xE9;mon that will become your partner on your journey, and a new bond will be born. You&#x27;ll also encounter incredibly tough Trainers during your journey. Visit Pok&#xE9;mon Gyms, where Pok&#xE9;mon Trainers gather and Gym Leaders stand ready for Trainers to challenge them.

			The Pok&#xE9;mon-Amie feature, which debuted in Pok&#xE9;mon X and Pok&#xE9;mon Y, enables players to get closer to their Pok&#xE9;mon by petting them, playing games with them, and feeding them. You can play with the Pok&#xE9;mon on your team at any time using Pok&#xE9;mon-Amie&#x2012;the more you care for your Pok&#xE9;mon in these ways, the deeper your bond will become. As your Pok&#xE9;mon become more affectionate toward you, they will be more likely to avoid attacks in battle, land critical hits on opponents, and perform more admirably than before.

			During your adventure, you&#x27;ll be able to create your very own Secret Base. Work on making a space that is all your own, just the way you like it. But that&#x27;s not even the best part! Using communication features, you&#x27;ll be able to easily connect with other players all around the world to share your Secret Bases with one another.

			You won&#x27;t raise your Pok&#xE9;mon just for battle in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire. In Pok&#xE9;mon Contest Spectaculars, your Pok&#xE9;mon will compete to see who has the most appeal! There are five kinds of contests that you can choose to enter: Coolness, Beauty, Cuteness, Cleverness, and Toughness. Your Pok&#xE9;mon will be judged on its appeal based on one of these five conditions. Choose a contest condition in which your Pok&#xE9;mon excels, and enter it in one of the corresponding contests!

			Enhance your Pok&#xE9;mon&#x27;s ability to perform by feeding it Pok&#xE9;blocks made from Berries. The type of Pok&#xE9;block you make is based on the color and variety of Berries you choose. When you feed them to your Pok&#xE9;mon, its contest conditions will improve according to the type of Pok&#xE9;block it eats!

			During your adventure, you will battle Team Magma in Pok&#xE9;mon Omega Ruby and Team Aqua in Pok&#xE9;mon Alpha Sapphire. Led by Maxie, Team Magma seeks to increase the land, while Archie and Team Aqua wish to increase the seas. In order to bring about these grand plans, each will turn to the power of a Legendary Pok&#xE9;mon: Groudon or Kyogre. Team Magma pursues Groudon, while Team Aqua goes after Kyogre. What could be driving them to such lengths?

			Look forward to encountering the Eon Pok&#xE9;mon Latios and Latias! In Pok&#xE9;mon Omega Ruby, you can team up with Latios, while Latias joins you in Pok&#xE9;mon Alpha Sapphire. And it has been discovered that Latios and Latias can also Mega Evolve, another exciting moment to anticipate!

			Partway through your adventure, you&#x27;ll become able to Soar in the sky. Gaze down over the wide Hoenn region as you Soar freely wherever you wish! Among the destinations you can reach using Soar are very special locations called Mirage spots. In Mirage spots, you may be able to find rare Pok&#xE9;mon that cannot be found elsewhere in the region, and you may be able to obtain valuable items.

			The Pok&#xE9;Nav Plus is your best friend on your journey. It&#x27;s a handy tool that will help you navigate your adventure as a Pok&#xE9;mon Trainer. It provides a lot of useful information, making your adventure easier and more fun than ever. Outside of battles and special events, the Pok&#xE9;Nav Plus will always be displayed on the lower screen of your Nintendo 3DS system. You can instantly switch between its four main functions with touch input, and be ready to play in no time.

			In Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire, a new story unfolds involving the Hoenn region&#x2012;the Delta Episode! This episode involves the history of the Hoenn region, reaching 3,000 years into the past, and makes the mysteries of Mega Evolution clear at long last. The troubled tale begins with Zinnia, a young woman who bears a certain destiny. The story includes appearances from Team Magma and Team Aqua, Steven, Gym Leader Wallace, and other familiar faces. Don&#x27;t miss the conflict between Rayquaza and Deoxys!

			Look for Mirage spots, where you may be able to find rare Pok&#xE9;mon that can&#x27;t be found elsewhere in the region and also obtain valuable items. Keep on the lookout for Legendary Pok&#xE9;mon that have recently been discovered at new Mirage spots. Note that the Legendary Pok&#xE9;mon you&#x27;ll encounter sometimes vary based on which game you&#x27;re playing. With Pok&#xE9;mon X, Pok&#xE9;mon Y, Pok&#xE9;mon Omega Ruby, and Pok&#xE9;mon Alpha Sapphire, every Legendary Pok&#xE9;mon discovered to date can be obtained!

			In Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire, you can obtain the Mega Stones needed to Mega Evolve all of the Mega-Evolving Pok&#xE9;mon that appeared in Pok&#xE9;mon X and Pok&#xE9;mon Y. Be sure to look high and low to find these mysterious and amazing items!

			Pok&#xE9;mon fans first enjoyed Pok&#xE9;mon Ruby Version and Pok&#xE9;mon Sapphire Version when it launched in 2003 for the Game Boy Advance. Now, a whole new generation of Pok&#xE9;mon fans will experience the dramatic storyline that unfolds in Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire.

			Pok&#xE9;mon Omega Ruby and Pok&#xE9;mon Alpha Sapphire are now available in stores and in Nintendo eShop.', N'https://pdvf3w.dm.files.1drv.com/y4mflanUltZ5Yja6pt2RskfkfZPOhCC592g8v34AxskeluAu_meIdAzNuUKKFphtx5XKQFigCfyOaMEdz5tqjJ1qL_D6IuMzYLAdyZv2ntWBqaYo1Ncc580eKzw9Wjv12YTsTZYp1uD6yyxQ5dmlRq-NnCbmNHeNXz4bHe6gcxcWRpV-nijtneMPCCKA2UYaArBhDJbN3CSoRZzewNvnlJztQ?width=1799&height=1601&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (26, N'Pok&#xE9;mon Sun', 7, N'Alola', N'2016-11-18', N'New Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-sun-and-pokemon-moon/', N'Your journey in Pok&#xE9;mon Sun and Pok&#xE9;mon Moon will take you across the beautiful islands of the Alola region, where you&#x27;ll encounter newly discovered Pok&#xE9;mon, as well as Pok&#xE9;mon that have taken on a new Alolan style. You may even encounter powerful Legendary Pok&#xE9;mon and other special Pok&#xE9;mon, such as the mysterious guardian deities. Keep track of all the Pok&#xE9;mon you&#x27;ve seen and caught with the new Rotom Pok&#xE9;dex.

			Some of the Pok&#xE9;mon you&#x27;ll train and battle with can learn powerful new Z-Moves,&#x2012;moves so strong they can be used only once in battle. There are Z-Moves for every different type, as well as exclusive Z-Moves for certain Pok&#xE9;mon, including Eevee and Pikachu. Try them out in battle to see what these awesome moves can do!

			Around every corner, your battling skills will be tested by tough Trainers! Epic battles are in store for you against Team Skull, a nefarious group of ruffians attempting to steal Pok&#xE9;mon, and you&#x27;ll also face the kahunas, the tough leaders of each island. And if you&#x27;re strong enough, you may reach the Battle Tree, a place where the most accomplished Trainers go to battle each other.

			The new Pok&#xE9;mon Refresh feature can keep your Pok&#xE9;mon in top shape after all that battling. Take care of your Pok&#xE9;mon by curing any status conditions like poisoning and paralysis. Plus, the more affectionate your Pok&#xE9;mon become toward you, the better they&#x27;ll perform in battle. Take good care of your Pok&#xE9;mon with Pok&#xE9;mon Refresh, and they&#x27;ll be great allies on your adventure!

			Your Pok&#xE9;mon can also enjoy a new experience known as Pok&#xE9; Pelago, a place for them to visit when they&#x27;ve been placed in PC Boxes. Pok&#xE9; Pelago is a group of islands where your Pok&#xE9;mon can explore, play, and do other fun activities. As your Pok&#xE9;mon play there, they might get stronger or obtain items for you.

			Using Pok&#xE9;mon Bank, you&#x27;ll be able to transfer Pok&#xE9;mon you&#x27;ve caught in the Nintendo 3DS Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow into your copy of Pok&#xE9;mon Sun or Pok&#xE9;mon Moon. Pok&#xE9;mon from Pok&#xE9;mon Omega Ruby, Pok&#xE9;mon Alpha Sapphire, Pok&#xE9;mon X, and Pok&#xE9;mon Y can also be brought into Pok&#xE9;mon Sun and Pok&#xE9;mon Moon in the same way.

			Look forward to all the places to explore, things to do, and Pok&#xE9;mon to discover in Pok&#xE9;mon Sun and Pok&#xE9;mon Moon!', N'https://qnvf3w.dm.files.1drv.com/y4mWvAbV4YUnubzA2vQACm_4PzQlPS4F8lnUEJBoJftZ1QSDLuKp0r761io4rVLHJCjnL-MXwMx6LZeed4QMdGBvkvqdfNULgcG4kY7nLDxOZFtCvqOmjOeLkQ1kHYFUMcFHH3_qHhg4pNjU8kCuY6c-uk8NgpamJ93EFnebDKC4PSdsmKUlfAqJgyNYSivA64-ieRv8npyOAeB6X2fBcZZzQ?width=1200&height=1095&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (27, N'Pok&#xE9;mon Moon', 7, N'Alola', N'2016-11-18', N'New Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-sun-and-pokemon-moon/', N'Your journey in Pok&#xE9;mon Sun and Pok&#xE9;mon Moon will take you across the beautiful islands of the Alola region, where you&#x27;ll encounter newly discovered Pok&#xE9;mon, as well as Pok&#xE9;mon that have taken on a new Alolan style. You may even encounter powerful Legendary Pok&#xE9;mon and other special Pok&#xE9;mon, such as the mysterious guardian deities. Keep track of all the Pok&#xE9;mon you&#x27;ve seen and caught with the new Rotom Pok&#xE9;dex.

			Some of the Pok&#xE9;mon you&#x27;ll train and battle with can learn powerful new Z-Moves,&#x2012;moves so strong they can be used only once in battle. There are Z-Moves for every different type, as well as exclusive Z-Moves for certain Pok&#xE9;mon, including Eevee and Pikachu. Try them out in battle to see what these awesome moves can do!

			Around every corner, your battling skills will be tested by tough Trainers! Epic battles are in store for you against Team Skull, a nefarious group of ruffians attempting to steal Pok&#xE9;mon, and you&#x27;ll also face the kahunas, the tough leaders of each island. And if you&#x27;re strong enough, you may reach the Battle Tree, a place where the most accomplished Trainers go to battle each other.

			The new Pok&#xE9;mon Refresh feature can keep your Pok&#xE9;mon in top shape after all that battling. Take care of your Pok&#xE9;mon by curing any status conditions like poisoning and paralysis. Plus, the more affectionate your Pok&#xE9;mon become toward you, the better they&#x27;ll perform in battle. Take good care of your Pok&#xE9;mon with Pok&#xE9;mon Refresh, and they&#x27;ll be great allies on your adventure!

			Your Pok&#xE9;mon can also enjoy a new experience known as Pok&#xE9; Pelago, a place for them to visit when they&#x27;ve been placed in PC Boxes. Pok&#xE9; Pelago is a group of islands where your Pok&#xE9;mon can explore, play, and do other fun activities. As your Pok&#xE9;mon play there, they might get stronger or obtain items for you.

			Using Pok&#xE9;mon Bank, you&#x27;ll be able to transfer Pok&#xE9;mon you&#x27;ve caught in the Nintendo 3DS Virtual Console versions of Pok&#xE9;mon Red, Pok&#xE9;mon Blue, and Pok&#xE9;mon Yellow into your copy of Pok&#xE9;mon Sun or Pok&#xE9;mon Moon. Pok&#xE9;mon from Pok&#xE9;mon Omega Ruby, Pok&#xE9;mon Alpha Sapphire, Pok&#xE9;mon X, and Pok&#xE9;mon Y can also be brought into Pok&#xE9;mon Sun and Pok&#xE9;mon Moon in the same way.

			Look forward to all the places to explore, things to do, and Pok&#xE9;mon to discover in Pok&#xE9;mon Sun and Pok&#xE9;mon Moon!', N'https://p9vf3w.dm.files.1drv.com/y4mXgu1cZ37fwWNmvrg3XjsiOq5FXrKl1UJ8gpxY5HO2U6a2-cGIk3vuWdaAIrns1Ea-eS3_pBg-J8Z_Xcy9gZPKHJvxN92IqL8GmdBgQ8uobrlIGOQZ8qLzl2-SlEesgG_NdhPozZY-TlmEaJV7z_r62bDDK9_DKftS7LygdpCE_1NstDHSp1jcIo6IgwbHz3948OIvKRmksdyn-4BBw0dww?width=1863&height=1704&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (28, N'Pok&#xE9;mon Ultra Sun', 7, N'Alola (Revisited)', N'2017-11-17', N'New Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-ultra-sun-and-pokemon-ultra-moon/', N'Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon have been powered up with new additions to the story and features of Pok&#xE9;mon Sun and Pok&#xE9;mon Moon. The games take place in the Alola region, which is made up of four islands, along with one artificial island. The islands are teeming with Pok&#xE9;mon, including some Alolan variants of familiar Pok&#xE9;mon that were originally discovered in the Kanto region. During your adventure, you&#x27;ll have multiple run-ins with the troublesome Team Skull, encounter the elusive guardian deities, and unravel a plot surrounding the mysterious Aether Foundation. There&#x27;s nothing like a return trip to the wonderful, tropical Alola region!

			The Mysteries of the Ultra Wormhole!
			A strange pocket of space has been spotted over the Alola region! The Ultra Wormhole is a gateway to other worlds, and occasionally, fearsome Ultra Beasts will emerge from it. These powerful creatures are unlike any that you&#x27;ve ever seen before. Challenge these incredible Ultra Beasts, and you might even be able to add them to your team. Just make sure to bring a few Beast Balls to catch them.

			With the help of the Legendary Pok&#xE9;mon Solgaleo or Lunala, you can even travel into the Ultra Wormhole to explore what lies beyond. You&#x27;ll find more Ultra Beasts within Ultra Space, but you may also encounter Legendary Pok&#xE9;mon such as Mewtwo, Ho-Oh, Lugia, and others. With enough persistence, you could potentially put together an entire team of Legendary Pok&#xE9;mon!

			Necrozma&#x27;s New Tale
			One Legendary Pok&#xE9;mon that you won&#x27;t have to travel to Ultra Space to meet is the mighty Necrozma. This imposing Pok&#xE9;mon features heavily in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon&#x27;s story, and by playing, you&#x27;ll uncover new secrets about Necrozma and the Alola region.

			Necrozma&#x27;s power continues to grow when it reveals its alternate forms&#x2012;Dusk Mane Necrozma appears in Pok&#xE9;mon Ultra Sun while Dawn Wings Necrozma shows up in Pok&#xE9;mon Ultra Moon. And just what is Ultra Necrozma? Can this Legendary Pok&#xE9;mon&#x27;s power be contained?

			Perhaps the Ultra Recon Squad will have the answer to that question. This group hails from a world that lies beyond an Ultra Wormhole, and they have come to the Alola region to find out more about Necrozma, including how to restore the light that the Legendary Pok&#xE9;mon has stolen from their home.

			Team Rainbow Rocket Strikes!
			Ultra Beasts aren&#x27;t the only challenge you&#x27;ll have while exploring the Alola region. A nefarious organization has resurfaced under a new guise&#x2012;Team Rainbow Rocket is here! Led by Giovanni, the boss of Team Rocket, this new group is composed of classic villains from previous games in the Pok&#xE9;mon series. What will you do when you&#x27;re forced to face off against the bosses of Team Magma, Team Aqua, Team Galactic, Team Plasma, and Team Flare? This could be your greatest test yet!

			Surf&#x27;s Up with Mantine Surf!
			But it&#x27;s not all battles against Ultra Beasts and past enemies in the Alola region. There&#x27;s always a little time for fun on your journey. What&#x27;s the point of an island paradise if you can&#x27;t shoot the curl? Mantine Surf is a most bodacious way to travel between the Alola region&#x27;s islands by hanging ten on the back of, yes, a Mantine. It&#x27;s also a cool competitive sport where you vie for the highest score by pulling off different radical moves. Excellent surfing skills will earn you BP (which here stands for Beach Points) that can be used to teach your Pok&#xE9;mon new moves&#x2012;moves that they would not be able to learn by leveling up or through any TM. You might even be able to net a special Pok&#xE9;mon!

			Enter the Battle Agency
			If you want a little more practice with Pok&#xE9;mon battling, be sure to check out the Battle Agency within Festival Plaza. This exciting new area allows you to compete in three-on-three Single Battles where you can earn Festival Coins and rare items like Rare Candy and Gold Bottle Caps. The only catch is that you can&#x27;t battle with your own Pok&#xE9;mon!

			Instead, you&#x27;ll need to borrow one Pok&#xE9;mon at the reception desk and the other two from fellow Trainers in your Festival Plaza. The better you battle, the stronger the Pok&#xE9;mon you&#x27;ll be able to borrow, so work hard and you can make it to the top of the Battle Agency!

			Tantalizing New Features
			There are other great new features in in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon. You&#x27;ll find some new trials on each of the Alola region&#x27;s four islands while taking the island challenge. These puzzles will require updated strategies to complete, and even if you do clear them, you&#x27;ll still have to face off against a powerful Totem Pok&#xE9;mon before the trial is cleared.

			A returning feature in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon is Z-Moves. With the use of your Z-Power Ring and a corresponding Z-Crystal, your Pok&#xE9;mon will be able to perform an exceptionally strong move&#x2012;one so powerful that it can only be used once per battle! This game introduces brand new Z-Moves, which will help make your team stronger than ever before.

			As you explore the Alola region, keep your eyes open for new Totem Stickers. These collectible goodies glitter and shine, and as you find them, be sure to show them off to Professor Samson Oak. The good professor will be so impressed by your diligence that he will reward you with special totem-like Pok&#xE9;mon as your collection grows.

			Pok&#xE9;mon Bank Compatibility
			Pok&#xE9;mon Bank is an application and service that allow you to deposit, store, and manage your Pok&#xE9;mon in private Boxes on the internet. Pok&#xE9;mon Bank is a paid service, with a small annual charge for usage.

			Pok&#xE9;mon Bank now works with Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon, making it easy to transfer Pok&#xE9;mon from copies of Pok&#xE9;mon Ultra Sun, Pok&#xE9;mon Ultra Moon, Pok&#xE9;mon Sun, and Pok&#xE9;mon Moon&#x2012;including the downloadable versions&#x2012;to a single game. You can also transfer Pok&#xE9;mon you&#x27;ve caught in the many previous Pok&#xE9;mon RPGs, including those on the Nintendo 3DS Virtual Console, into Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon. It&#x27;s an easy and efficient way to complete your Pok&#xE9;dex and raise multiple Pok&#xE9;mon for use in battles and competitions.

			With so much to see and do (and so many Pok&#xE9;mon to catch!), there are plenty of reasons to book another trip to the Alola region with Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon.', N'https://stvf3w.dm.files.1drv.com/y4mO7i_AG8AL_I6BgRxx3eU0oMzFZst8aqQXNkWukQE0-zoCt0RfOR9Ih4tXv-wLWkVIGG4BHHi-NqWhXlspo-ThBzsyTE9MqiBG6vd7R6Llm9dFLKM4tSQN8YKR8aU-pS1PxCpcCDWiwYnjs8AaSRkbu6tg83xaYYqeYt3hhwWEgIMd_2szr5gN7NSL8xzeUBTtJa-CgbcQKMCrw_-wjuBDw?width=1200&height=1095&cropmode=none')
			INSERT INTO dbo.GameData (ID, GameName, Generation, Region, DateReleased, [Platform], WebsiteURL, [Description], CoverArtImageURL) VALUES (29, N'Pok&#xE9;mon Ultra Moon', 7, N'Alola (Revisited)', N'2017-11-17', N'New Nintendo 3DS/2DS', N'https://www.pokemon.com/us/pokemon-video-games/pokemon-ultra-sun-and-pokemon-ultra-moon/', N'Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon have been powered up with new additions to the story and features of Pok&#xE9;mon Sun and Pok&#xE9;mon Moon. The games take place in the Alola region, which is made up of four islands, along with one artificial island. The islands are teeming with Pok&#xE9;mon, including some Alolan variants of familiar Pok&#xE9;mon that were originally discovered in the Kanto region. During your adventure, you&#x27;ll have multiple run-ins with the troublesome Team Skull, encounter the elusive guardian deities, and unravel a plot surrounding the mysterious Aether Foundation. There&#x27;s nothing like a return trip to the wonderful, tropical Alola region!

			The Mysteries of the Ultra Wormhole!
			A strange pocket of space has been spotted over the Alola region! The Ultra Wormhole is a gateway to other worlds, and occasionally, fearsome Ultra Beasts will emerge from it. These powerful creatures are unlike any that you&#x27;ve ever seen before. Challenge these incredible Ultra Beasts, and you might even be able to add them to your team. Just make sure to bring a few Beast Balls to catch them.

			With the help of the Legendary Pok&#xE9;mon Solgaleo or Lunala, you can even travel into the Ultra Wormhole to explore what lies beyond. You&#x27;ll find more Ultra Beasts within Ultra Space, but you may also encounter Legendary Pok&#xE9;mon such as Mewtwo, Ho-Oh, Lugia, and others. With enough persistence, you could potentially put together an entire team of Legendary Pok&#xE9;mon!

			Necrozma&#x27;s New Tale
			One Legendary Pok&#xE9;mon that you won&#x27;t have to travel to Ultra Space to meet is the mighty Necrozma. This imposing Pok&#xE9;mon features heavily in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon&#x27;s story, and by playing, you&#x27;ll uncover new secrets about Necrozma and the Alola region.

			Necrozma&#x27;s power continues to grow when it reveals its alternate forms&#x2012;Dusk Mane Necrozma appears in Pok&#xE9;mon Ultra Sun while Dawn Wings Necrozma shows up in Pok&#xE9;mon Ultra Moon. And just what is Ultra Necrozma? Can this Legendary Pok&#xE9;mon&#x27;s power be contained?

			Perhaps the Ultra Recon Squad will have the answer to that question. This group hails from a world that lies beyond an Ultra Wormhole, and they have come to the Alola region to find out more about Necrozma, including how to restore the light that the Legendary Pok&#xE9;mon has stolen from their home.

			Team Rainbow Rocket Strikes!
			Ultra Beasts aren&#x27;t the only challenge you&#x27;ll have while exploring the Alola region. A nefarious organization has resurfaced under a new guise&#x2012;Team Rainbow Rocket is here! Led by Giovanni, the boss of Team Rocket, this new group is composed of classic villains from previous games in the Pok&#xE9;mon series. What will you do when you&#x27;re forced to face off against the bosses of Team Magma, Team Aqua, Team Galactic, Team Plasma, and Team Flare? This could be your greatest test yet!

			Surf&#x27;s Up with Mantine Surf!
			But it&#x27;s not all battles against Ultra Beasts and past enemies in the Alola region. There&#x27;s always a little time for fun on your journey. What&#x27;s the point of an island paradise if you can&#x27;t shoot the curl? Mantine Surf is a most bodacious way to travel between the Alola region&#x27;s islands by hanging ten on the back of, yes, a Mantine. It&#x27;s also a cool competitive sport where you vie for the highest score by pulling off different radical moves. Excellent surfing skills will earn you BP (which here stands for Beach Points) that can be used to teach your Pok&#xE9;mon new moves&#x2012;moves that they would not be able to learn by leveling up or through any TM. You might even be able to net a special Pok&#xE9;mon!

			Enter the Battle Agency
			If you want a little more practice with Pok&#xE9;mon battling, be sure to check out the Battle Agency within Festival Plaza. This exciting new area allows you to compete in three-on-three Single Battles where you can earn Festival Coins and rare items like Rare Candy and Gold Bottle Caps. The only catch is that you can&#x27;t battle with your own Pok&#xE9;mon!

			Instead, you&#x27;ll need to borrow one Pok&#xE9;mon at the reception desk and the other two from fellow Trainers in your Festival Plaza. The better you battle, the stronger the Pok&#xE9;mon you&#x27;ll be able to borrow, so work hard and you can make it to the top of the Battle Agency!

			Tantalizing New Features
			There are other great new features in in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon. You&#x27;ll find some new trials on each of the Alola region&#x27;s four islands while taking the island challenge. These puzzles will require updated strategies to complete, and even if you do clear them, you&#x27;ll still have to face off against a powerful Totem Pok&#xE9;mon before the trial is cleared.

			A returning feature in Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon is Z-Moves. With the use of your Z-Power Ring and a corresponding Z-Crystal, your Pok&#xE9;mon will be able to perform an exceptionally strong move&#x2012;one so powerful that it can only be used once per battle! This game introduces brand new Z-Moves, which will help make your team stronger than ever before.

			As you explore the Alola region, keep your eyes open for new Totem Stickers. These collectible goodies glitter and shine, and as you find them, be sure to show them off to Professor Samson Oak. The good professor will be so impressed by your diligence that he will reward you with special totem-like Pok&#xE9;mon as your collection grows.

			Pok&#xE9;mon Bank Compatibility
			Pok&#xE9;mon Bank is an application and service that allow you to deposit, store, and manage your Pok&#xE9;mon in private Boxes on the internet. Pok&#xE9;mon Bank is a paid service, with a small annual charge for usage.

			Pok&#xE9;mon Bank now works with Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon, making it easy to transfer Pok&#xE9;mon from copies of Pok&#xE9;mon Ultra Sun, Pok&#xE9;mon Ultra Moon, Pok&#xE9;mon Sun, and Pok&#xE9;mon Moon&#x2012;including the downloadable versions&#x2012;to a single game. You can also transfer Pok&#xE9;mon you&#x27;ve caught in the many previous Pok&#xE9;mon RPGs, including those on the Nintendo 3DS Virtual Console, into Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon. It&#x27;s an easy and efficient way to complete your Pok&#xE9;dex and raise multiple Pok&#xE9;mon for use in battles and competitions.

			With so much to see and do (and so many Pok&#xE9;mon to catch!), there are plenty of reasons to book another trip to the Alola region with Pok&#xE9;mon Ultra Sun and Pok&#xE9;mon Ultra Moon.', N'https://sdvf3w.dm.files.1drv.com/y4mF61QW1KqBwnn3aXkRhBjGVVTnHit0UsDCXmNiq2QaLp9X_-2ZqNSuDZ957Yea0sl8JxX8LLBuzeLgZ_6v_mxOboExkFSkBx0rleZNLAuEtSFjT-lFvOoq2Bo2UEQLRv5ZD0HQSUI2JfcyCl3AUzpmhB-4yTB-T_-UgNhaYD9UvMeiccYbs7YH1EuT_Bikmah8lUHU0iqfDBD9co9cfLyhw?width=300&height=275&cropmode=none')
			SET IDENTITY_INSERT dbo.GameData OFF
			
			
			SET IDENTITY_INSERT dbo.ROMFiles ON
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (1, 1, N'Pokemon Red.gb', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143739&authkey=AEBjjCXP6fuIUx4')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (2, 2, N'Pokemon Blue.gb', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143741&authkey=AKob7NoV5oYkZ04')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (3, 3, N'Pokemon Green (JP).gb', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143752&authkey=ANNp7e-U1DmxtR4')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (4, 4, N'Pokemon Yellow (Special Pikachu Edition).gb', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143942&authkey=AC_roGd_puo57PA')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (5, 5, N'Pokemon Gold.gbc', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143735&authkey=APsSuvOpTZLa478')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (6, 6, N'Pokemon Silver.gbc', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143732&authkey=AKIBsjZ9_3fW4J4')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (7, 7, N'Pokemon Crystal.gbc', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143737&authkey=ADC95wARwN_eJPc')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (8, 8, N'Pokemon Ruby.gba', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143730&authkey=ACBlDIIkXCqoptM')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (9, 9, N'Pokemon Sapphire.gba', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143728&authkey=AGrz0ZIMuraQLpk')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (10, 10, N'Pokemon Emerald.gba', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143740&authkey=AIoUF0d8yxrF8uE')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (11, 11, N'Pokemon FireRed.gba', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143733&authkey=AElcO4dMWij9-R0')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (12, 12, N'Pokemon LeafGreen.gba', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143729&authkey=AA3VkVxbAConY9U')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (13, 13, N'Pokemon Diamond.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143727&authkey=AK5k0Z5UOBjuTaE')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (14, 14, N'Pokemon Pearl.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143731&authkey=AC_IxO-ftz0_b4g')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (15, 15, N'Pokemon Platinum.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143725&authkey=AMrM5umXV-UKgjQ')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (16, 16, N'Pokemon HeartGold.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143726&authkey=AHTzLRkYbhoCBhI')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (17, 17, N'Pokemon SoulSilver.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143724&authkey=AKQvIVxo_rnPWik')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (18, 18, N'Pokemon Black.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143722&authkey=AFNwOS-6992_y0E')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (19, 19, N'Pokemon White.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143720&authkey=APynwlV3kGAW-uo')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (20, 20, N'Pokemon Black 2.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143754&authkey=ADUN-rcvZCkeeEk')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (21, 21, N'Pokemon White 2.nds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143723&authkey=AK7CubCnVc7e4uU')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (22, 22, N'Pokemon X.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143714&authkey=AOKx4LSnlQgwPTk')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (23, 23, N'Pokemon Y.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143715&authkey=AHEtXz31Fr7e7c4')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (24, 24, N'Pokemon Omega Ruby.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143717&authkey=AHrHOYARI_bdPlE')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (25, 25, N'Pokemon Alpha Sapphire.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143719&authkey=AOKcHndPV9N7PcM')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (26, 26, N'Pokemon Sun.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143713&authkey=AJ14WeT6lwRff0k')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (27, 27, N'Pokemon Moon.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143716&authkey=AMPIdZapK75cHg8')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (28, 28, N'Pokemon Ultra Sun.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143910&authkey=AAuLH14pM6MmpT0')
			INSERT INTO dbo.ROMFiles (ID, GameID, [FileName], DownloadLink) VALUES (29, 29, N'Pokemon Ultra Moon.3ds', N'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143712&authkey=ADBPbJ8YuEzb6JM')
			SET IDENTITY_INSERT dbo.ROMFiles OFF
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
		IF @@rowcount > 0
			ROLLBACK TRAN
	END CATCH
END
GO

PRINT 'Finished Creating `spInsertCorePokemonROMs` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `spUpdate_ReseedTables` Stored Procedure...'
GO

CREATE PROCEDURE dbo.spPartialUpdateReseedTables
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			IF (dbo.fnCheckIfTablesHaveRows() = 0)
			BEGIN
				ALTER TABLE dbo.ROMFiles DROP CONSTRAINT [FK__ROMFiles__GameData]
				IF (dbo.fnCheckIfCurrentIdentInTablesIsNot1() = 1)
				BEGIN
					DBCC CHECKIDENT('dbo.GameData', RESEED, 0)
					DBCC CHECKIDENT('dbo.ROMFiles', RESEED, 0)
				END
				ELSE
				BEGIN
					DBCC CHECKIDENT('dbo.GameData', RESEED, 1)
					DBCC CHECKIDENT('dbo.ROMFiles', RESEED, 1)
				END
				ALTER TABLE dbo.ROMFiles WITH CHECK ADD CONSTRAINT [FK__ROMFiles__GameData] FOREIGN KEY(GameID)
				REFERENCES dbo.GameData (ID)
				ALTER TABLE dbo.ROMFiles CHECK CONSTRAINT [FK__ROMFiles__GameData]
			END
			ELSE
				RAISERROR('Error: Canont reseed database tables when there are rows present.', 16, 1)
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		SELECT  
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,  
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage; 
		IF @@ERROR != 0
		BEGIN
			PRINT 'An error occured!'
			ROLLBACK TRAN
		END
	END CATCH
END
GO

PRINT 'Finished Creating `spUpdate_ReseedTables` Stored Procedure!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Finished Creating All Stored Procedures!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating Functions...'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `fnReadAllPokemonROMs` Table-Valued Function...' 
GO

CREATE FUNCTION dbo.fnReadAllPokemonROMs()
RETURNS TABLE
AS
RETURN (SELECT * FROM dbo.PokemonROMs)
GO

PRINT 'Finished Creating `fnReadAllPokemonROMs` Table-Valued Function!'
PRINT CHAR(13) + CHAR(10)
GO 

PRINT 'Creating `fnGetLatestGameDataID` Scalar-Valued Function...'
GO

CREATE FUNCTION dbo.fnGetLatestGameDataID()  
RETURNS INT   
AS
BEGIN
	DECLARE @Latest_ID INT = (SELECT ID FROM GameData WHERE ID = IDENT_CURRENT('dbo.GameData'))
	RETURN @Latest_ID
END
GO

PRINT 'Finished Creating `fnGetLatestGameDataID` Scalar-Valued Function!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `fnCheckIfCurrentIdentInTablesIsNot1` Scalar-Valued Function...'
GO

CREATE FUNCTION dbo.fnCheckIfCurrentIdentInTablesIsNot1()
RETURNS BIT
AS
BEGIN
	DECLARE @Current_Ident_GameData_Table INT = (SELECT IDENT_CURRENT('dbo.ROMFiles'))
	DECLARE @Current_Ident_ROMFiles_Table INT = (SELECT IDENT_CURRENT('dbo.GameData'))
	IF (@Current_Ident_GameData_Table != 1 AND @Current_Ident_ROMFiles_Table != 1)
		RETURN CAST(1 AS bit)
	ELSE
		RETURN CAST(0 AS bit)
	RETURN NULL
END
GO

PRINT 'Finished Creating `fnCheckIfCurrentIdentInTablesIsNot1` Scalar-Valued Function!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Creating `fnCheckIfTablesHaveRows` Scalar-Valued Function...'
GO

CREATE FUNCTION dbo.fnCheckIfTablesHaveRows()
RETURNS BIT
AS
BEGIN
	DECLARE @Row_Count_GameData_Table INT = (SELECT COUNT(*) FROM dbo.ROMFiles)
	DECLARE @Row_Count_ROMFiles_Table INT = (SELECT COUNT(*) FROM dbo.GameData)
	IF (@Row_Count_GameData_Table = 0 AND @Row_Count_ROMFiles_Table = 0)
		RETURN CAST(0 AS bit)
	ELSE
		RETURN CAST(1 AS bit)
	RETURN NULL
END
GO

PRINT 'Finished Creating `fnCheckIfTablesHaveRows` Scalar-Valued Function!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Finished Creating All Functions!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Inserting All Core Pokemon Games...'
GO

EXEC dbo.spInsertCorePokemonROMs
GO

PRINT CHAR(13) + CHAR(10)
PRINT 'Finished Inserting All Core Pokemon Games!'
PRINT CHAR(13) + CHAR(10)
GO

PRINT 'Done Running Script!'
GO

-- End

USE PokemonROMsDB
GO

-- READ SAMPLE ROM DATA

DECLARE @Pokemon_Ranger_ID INT = (
	SELECT GameID FROM dbo.PokemonROMs WHERE 
	GameName = 'Pok&#xE9;mon Ranger' AND
	Generation = 4 AND
	DateReleased = '2006-03-23' AND
	[Platform] = 'Nintendo DS Phat' AND
	WebsiteURL = 'https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/' AND
	[Description] = 'Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27;s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!' AND
	CoverArtImageURL = 'https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en' AND
	[FileName] = 'Pokemon Ranger.nds' AND 
	DownloadLink = 'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0'
)

EXEC dbo.spSelectPokemonROM @Pokemon_Ranger_ID
GO

-- CREATE SAMPLE ROM DATA

EXEC dbo.spInsertPokemonROM 
'Pok&#xE9;mon Ranger', 
4, 
'Hoenn', 
'2006-03-23', 
'Nintendo DS Phat', 
'https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/', 
'Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27;s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!', 
'https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en', 
'Pokemon Ranger.nds', 
'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0'
GO

-- UPDATE SAMPLE ROM DATA

DECLARE @Pokemon_Ranger_ID INT = (
	SELECT GameID FROM dbo.PokemonROMs WHERE 
	GameName = 'Pok&#xE9;mon Ranger' AND
	Generation = 4 AND
	DateReleased = '2006-03-23' AND
	[Platform] = 'Nintendo DS Phat' AND
	WebsiteURL = 'https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/' AND
	[Description] = 'Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27;s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!' AND
	CoverArtImageURL = 'https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en' AND
	[FileName] = 'Pokemon Ranger.nds' AND 
	DownloadLink = 'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0'
)

EXEC dbo.spUpdatePokemonROM 
@Pokemon_Ranger_ID,
'Pok&#xE9;mon Ranger', 
4, 
'Hoenn', 
'2006-03-23', 
'Nintendo DS Lite', 
'https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/', 
'Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27;s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!', 
'https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en', 
'Pokemon Ranger.nds', 
'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0'
GO

-- DELETE SAMPLE ROM DATA

DECLARE @Pokemon_Ranger_ID INT = (
	SELECT GameID FROM dbo.PokemonROMs WHERE 
	GameName = 'Pok&#xE9;mon Ranger' AND
	Generation = 4 AND
	DateReleased = '2006-03-23' AND
	[Platform] = 'Nintendo DS Phat' AND
	WebsiteURL = 'https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/' AND
	[Description] = 'Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27;s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!' AND
	CoverArtImageURL = 'https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en' AND
	[FileName] = 'Pokemon Ranger.nds' AND 
	DownloadLink = 'https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0'
)

EXEC dbo.spDeletePokemonROM @Pokemon_Ranger_ID
GO

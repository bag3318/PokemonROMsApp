USE PokemonROMsDB
GO

SELECT * FROM dbo.fnReadAllPokemonROMs()
ORDER BY GameID DESC
GO


-- ASC = Ascending Order
-- DESC = Descending Order

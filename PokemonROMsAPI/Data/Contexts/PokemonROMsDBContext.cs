﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Domain.Models;
using Domain.ViewModel;

namespace Data.Contexts
{
    public /*partial*/ class PokemonROMsDBContext : DbContext
    {
        public IConfiguration Configuration { get; }

        public PokemonROMsDBContext(DbContextOptions<PokemonROMsDBContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("PokemonROMsDB"));
        }

        // DB Tables
        public virtual DbSet<GameData> GameData { get; set; }
        public virtual DbSet<RomFile> RomFiles { get; set; }

        // DB View
        public virtual DbSet<PokemonROM> PokemonROMs { get; set; }

        // Create Fluent API
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // since our models have a required property that is their counterpart, we must use the generic
            // `object` datatype for our db seeding data (the `var` keyword works too, but object is more consistant).
            object gameDataSeed = new
            {
                Id = 1,
                GameName = "Pok&#xE9;mon Ranger",
                Generation = (byte)4,
                Region = "Hoenn",
                DateReleased = new DateTime(2006, 03, 23),
                Description = "Pok&#xE9;mon Ranger returns as a Virtual Console game on Wii U. Now&#x27s your chance to battle with Pok&#xE9;mon in a unique and innovative way as you take on the nefarious Go-Rock Squad! Look for Pok&#xE9;mon Ranger in the Wii U Nintendo eShop. Visit a cool region and experience a different way to interact with Pok&#xE9;mon in the Pok&#xE9;mon Ranger game for the Nintendo DS system. Join the Pok&#xE9;mon Rangers and help maintain harmony between humans and Pok&#xE9;mon with the help of the Capture Styler. But look out for the Go-Rock Squad&#x2012;they want to use Pok&#xE9;mon for a more sinister purpose, and it&#x27;s up to you to help stop them. It&#x27;s an adventure that gets you into the Pok&#xE9;mon action like never before! Don&#x27;t worry about having to go it alone though&#x2012;you&#x27;ll have either Minun or Plusle at your side for the whole quest, depending on whether you choose to be a male or female Pok&#xE9;mon Ranger. As a rookie Pok&#xE9;mon Ranger, you&#x27;ll use a unique device to befriend Pok&#xE9;mon and ask them for their assistance. Use the stylus to circle wild Pok&#xE9;mon via the Capture Styler. Once they&#x27;re on your side, use their special abilities, such as tackling or shooting fire, to help you trap and befriend other Pok&#xE9;mon. With an army of cool and powerful Pok&#xE9;mon at your side, you&#x27;ll be completing vital missions in no time!",
                Platform = "Nintendo DS Phat",
                WebsiteUrl = "https://www.pokemon.com/us/pokemon-video-games/pokemon-ranger/",
                CoverArtImageUrl = "https://vignette.wikia.nocookie.net/nintendo/images/f/f6/Pokemon_Ranger_%28NA%29.png/revision/latest?cb=20131110022740&path-prefix=en",
                RomFileId = 1
            };

            object romFileSeed = new
            {
                Id = 1,
                GameId = 1,
                DownloadLink = "https://onedrive.live.com/download?cid=093DC4D54812866B&resid=93DC4D54812866B%2143900&authkey=ACoUvpUu6r1Vau0",
                FileName = "Pokemon Ranger.nds"
            };

            modelBuilder.Entity<GameData>(entity =>
            {
                entity.ToTable("GameData");

                entity.HasKey(g => g.Id);

                entity.Property(g => g.Id)
                    .HasColumnName("ID")
                    .HasColumnType("INT")
                    .ValueGeneratedOnAdd();

                entity.Property(g => g.GameName)
                    .IsRequired(true)
                    .HasColumnName("GameName")
                    .HasColumnType("NVARCHAR(44)")
                    .HasMaxLength(44);

                entity.Property(g => g.CoverArtImageUrl)
                    .IsRequired(true)
                    .HasColumnName("CoverArtImageURL")
                    .HasColumnType("NVARCHAR(400)")
                    .HasMaxLength(400);

                entity.Property(g => g.DateReleased)
                    .IsRequired(true)
                    .HasColumnName("DateReleased")
                    .HasColumnType("DATE");

                entity.Property(g => g.Description)
                    .IsRequired(true)
                    .HasColumnName("Description")
                    .HasColumnType("NVARCHAR(MAX)")
                    .HasMaxLength(4000);

                entity.Property(g => g.Generation)
                    .IsRequired(true)
                    .HasColumnName("Generation")
                    .HasColumnType("TINYINT");

                entity.Property(g => g.Platform)
                    .IsRequired(true)
                    .HasColumnName("Platform")
                    .HasColumnType("NVARCHAR(23)")
                    .HasMaxLength(23);

                entity.Property(g => g.Region)
                    .IsRequired(true)
                    .HasColumnName("Region")
                    .HasColumnType("NVARCHAR(23)")
                    .HasMaxLength(23);

                entity.Property(g => g.WebsiteUrl)
                    .IsRequired(true)
                    .HasColumnName("WebsiteURL")
                    .HasColumnType("NVARCHAR(400)")
                    .HasMaxLength(400);

                // seed the db
                entity.OwnsOne(r => r.RomFile).HasData(romFileSeed);
            });

            modelBuilder.Entity<RomFile>(entity =>
            {
                entity.ToTable("ROMFiles");

                entity.HasKey(r => r.Id);

                entity.HasAlternateKey(r => r.GameId);

                entity.Property(r => r.Id)
                    .HasColumnName("ID")
                    .HasColumnType("INT")
                    .ValueGeneratedOnAdd();

                entity.Property(r => r.GameId)
                    .HasColumnName("GameID")
                    .HasColumnType("INT");

                entity.Property(r => r.DownloadLink)
                    .IsRequired(true)
                    .HasColumnName("DownloadLink")
                    .HasColumnType("NVARCHAR(400)")
                    .HasMaxLength(400);

                entity.Property(r => r.FileName)
                    .IsRequired(true)
                    .HasColumnName("FileName")
                    .HasColumnType("NVARCHAR(50)")
                    .HasMaxLength(50);

                entity.HasOne(g => g.GameData)
                    .WithOne(r => r.RomFile)
                    .HasForeignKey<RomFile>(r => r.GameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ROMFiles__GameData");

                // seed the db
                entity.OwnsOne(g => g.GameData).HasData(gameDataSeed);
            });
        }
    }
}

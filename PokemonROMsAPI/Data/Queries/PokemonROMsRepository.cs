﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Data.Contexts;
using Data.Modifications;
using Domain.Models;
using Domain.IQueries;
using Domain.ViewModel;

namespace Data.Queries
{
    public class PokemonROMsRepository : IPokemonROMsRepository
    {
        private readonly ILogger _logger;
        private readonly PokemonROMsDBContext _context;

        public PokemonROMsRepository(PokemonROMsDBContext context, ILoggerFactory logger)
        {
            _context = context;
            _logger = logger.CreateLogger(ToString()); // ToString() = this.ToString()
        }

        public bool DataExists(int id = 0)
        {
            try
            {
                bool gameDataExists;
                bool romFileExists;

                if (id != 0)
                {
                    gameDataExists = _context.GameData.Any(g => g.Id == id);
                    romFileExists = _context.RomFiles.Any(r => r.GameId == id);
                }
                else
                {
                    gameDataExists = _context.GameData.Any();
                    romFileExists = _context.GameData.Any();
                }

                return gameDataExists && romFileExists ? true : false;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task<List<PokemonROM>> ReadAllPokemonROMsAsync()
        {
            try
            {
                RawSqlString sqlQuery = "SELECT * FROM dbo.fnReadAllPokemonROMs() ORDER BY GameID DESC";
                List<PokemonROM> pokemonROMsList = await _context.PokemonROMs.FromSql(sql: sqlQuery).ToListAsync();
                return pokemonROMsList;
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task<PokemonROM> ReadSinglePokemonROMAsync(int id)
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spSelectPokemonROM @id";
                SqlParameter idParam = new SqlParameter("id", id);
                PokemonROM pokemonROM = await _context.PokemonROMs.FromSql(sql: sqlQuery, parameters: idParam)
                    .SingleOrDefaultAsync();
                return pokemonROM;
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task<PokemonROM> CreatePokemonROMAsync(GameData gameData)
        {
            try
            {
                ValueTuple<RawSqlString, RawSqlString> sqlQueries = new ValueTuple<RawSqlString, RawSqlString>(
                    "EXEC dbo.spInsertPokemonROM @game_name, @generation, @region, @date_released, @platform, @website_url, @description, @cover_art_image_url, @file_name, @download_link", 
                    "SELECT * FROM dbo.PokemonROMs WHERE GameID = dbo.fnGetLatestTodoID()");

                RawSqlString createQuery = sqlQueries.Item1;
                RawSqlString selectQuery = sqlQueries.Item2;

               var parameterizedData = Parameterization.ParameterizeData(gameData);

                await _context.Database.ExecuteSqlCommandAsync(sql: createQuery,
                    parameters: new SqlParameter[]
                    {
                        parameterizedData.GameNameParam,
                        parameterizedData.GenerationParam,
                        parameterizedData.RegionParam,
                        parameterizedData.DateReleasedParam,
                        parameterizedData.PlatformParam,
                        parameterizedData.WebsiteUrlParam,
                        parameterizedData.DescriptionParam,
                        parameterizedData.CoverArtImageUrlParam,
                        parameterizedData.FileNameParam,
                        parameterizedData.DownloadLinkParam
                    });

                PokemonROM pokemonROM = await _context.PokemonROMs.FromSql(sql: selectQuery).FirstOrDefaultAsync();
                return pokemonROM;
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task<List<PokemonROM>> CreateCorePokemonROMsAsync()
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spInsertCorePokemonROMs";
                await _context.Database.ExecuteSqlCommandAsync(sql: sqlQuery);
                List<PokemonROM> corePokemonROMsList = await ReadAllPokemonROMsAsync();
                return corePokemonROMsList;
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task<PokemonROM> UpdatePokemonROMAsync(int id, GameData gameData)
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spUpdatePokemonROM @id, @game_name, @generation, @region, @date_released, @platform, @website_url, @description, @cover_art_image_url, @file_name, @download_link";

                SqlParameter idParam = new SqlParameter("id", id);
                ParameterizedData parameterizedData = Parameterization.ParameterizeData(gameData);

                await _context.Database.ExecuteSqlCommandAsync(sql: sqlQuery,
                    parameters: new SqlParameter[]
                    {
                        idParam,
                        parameterizedData.GameNameParam,
                        parameterizedData.GenerationParam,
                        parameterizedData.RegionParam,
                        parameterizedData.DateReleasedParam,
                        parameterizedData.PlatformParam,
                        parameterizedData.WebsiteUrlParam,
                        parameterizedData.DescriptionParam,
                        parameterizedData.CoverArtImageUrlParam,
                        parameterizedData.FileNameParam,
                        parameterizedData.DownloadLinkParam
                    });

                PokemonROM pokemonROM = await ReadSinglePokemonROMAsync(id);
                return pokemonROM;
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task RemovePokemonROMAsync(int id)
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spDeletePokemonROM @id";
                SqlParameter idParam = new SqlParameter("id", id);
                await _context.Database.ExecuteSqlCommandAsync(sql: sqlQuery, parameters: idParam);
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task RemoveAllPokemonROMsAsync()
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spDeleteAllPokemonROMs";
                await _context.Database.ExecuteSqlCommandAsync(sql: sqlQuery);
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }

        public async Task PartialUpdateReseedTablesAsync()
        {
            try
            {
                RawSqlString sqlQuery = "EXEC dbo.spPartialUpdateReseedTables";
                await _context.Database.ExecuteSqlCommandAsync(sql: sqlQuery);
            }
            catch (SqlException sqlException)
            {
                string sqlErr = sqlException.Message;
                _logger.LogError(sqlErr);
                Debug.WriteLine(sqlErr);
                throw sqlException;
            }
            catch (Exception exception)
            {
                string err = exception.Message;
                _logger.LogError(err);
                Debug.WriteLine(err);
                throw exception;
            }
        }
    }
}

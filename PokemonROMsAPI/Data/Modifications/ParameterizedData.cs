﻿using System.Data.SqlClient;

namespace Data.Modifications
{
    public class ParameterizedData
    {
        internal SqlParameter gameNameParam;
        public SqlParameter GameNameParam
        {
            get { return gameNameParam; }
            set { gameNameParam = value; }
        }

        internal SqlParameter generationParam;
        public SqlParameter GenerationParam
        {
            get { return generationParam; }
            set { generationParam = value; }
        }

        internal SqlParameter regionParam;
        public SqlParameter RegionParam
        {
            get { return regionParam; }
            set { regionParam = value; }
        }

        internal SqlParameter dateReleasedParam;
        public SqlParameter DateReleasedParam
        {
            get { return dateReleasedParam; }
            set { dateReleasedParam = value; }
        }

        internal SqlParameter platformParam;
        public SqlParameter PlatformParam
        {
            get { return platformParam; }
            set { platformParam = value; }
        }

        internal SqlParameter websiteUrlParam;
        public SqlParameter WebsiteUrlParam
        {
            get { return websiteUrlParam; }
            set { websiteUrlParam = value; }
        }

        internal SqlParameter descriptionParam;
        public SqlParameter DescriptionParam
        {
            get { return descriptionParam; }
            set { descriptionParam = value; }
        }

        internal SqlParameter coverArtImageUrlParam;
        public SqlParameter CoverArtImageUrlParam
        {
            get { return coverArtImageUrlParam; }
            set { coverArtImageUrlParam = value; }
        }

        internal SqlParameter fileNameParam;
        public SqlParameter FileNameParam
        {
            get { return fileNameParam; }
            set { fileNameParam = value; }
        }

        internal SqlParameter downloadLinkParam;
        public SqlParameter DownloadLinkParam
        {
            get { return downloadLinkParam; }
            set { downloadLinkParam = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Domain.Models;
using Sanitation;

namespace Data.Modifications
{
    public static class Parameterization
    {
        public static ParameterizedData ParameterizeData(GameData gameData)
        {
            ParameterizedData parameterizedData = new ParameterizedData
            {
                GameNameParam = new SqlParameter(
                    "game_name", gameData.GameName.SanitizeXSS().SanitizeSQLInjection()),
                GenerationParam = new SqlParameter(
                    "generation", Convert.ToByte(
                        gameData.Generation.ToString().SanitizeXSS().SanitizeSQLInjection())),
                RegionParam = new SqlParameter(
                    "region", gameData.Region.SanitizeXSS().SanitizeSQLInjection()),
                DateReleasedParam = new SqlParameter(
                    "date_released", Convert.ToDateTime(
                        gameData.DateReleased.ToString().Replace('/', '-').SanitizeXSS().SanitizeSQLInjection())),
                PlatformParam = new SqlParameter(
                    "platform", gameData.Platform.SanitizeXSS().SanitizeSQLInjection()),
                WebsiteUrlParam = new SqlParameter(
                    "website_url", gameData.WebsiteUrl.EncodeUrl()),
                DescriptionParam = new SqlParameter(
                    "description", gameData.Description.SanitizeXSS().SanitizeSQLInjection()),
                CoverArtImageUrlParam = new SqlParameter(
                    "cover_art_image_url", gameData.CoverArtImageUrl.EncodeUrl()),
                FileNameParam = new SqlParameter(
                    "file_name", gameData.RomFile.FileName.SanitizeXSS().SanitizeSQLInjection()),
                DownloadLinkParam = new SqlParameter(
                    "download_link", gameData.RomFile.DownloadLink.EncodeUrl())
            };

            return parameterizedData;
        }
    }
}

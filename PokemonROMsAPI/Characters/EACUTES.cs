﻿namespace Characters
{
    /**
     * <summary>
     * EACUTES Character Class, includes big and little EACUTEs
     * </summary>
     */
    public sealed class EACUTES
    {
        /**
         * <summary>
         * Represents a character that contains the accented-little-e seen in the word "Pokemon".
         * </summary>
         */
        public static readonly char EACUTE_LITTLE = '\xE9';

        /**
         * <summary>
         * Represents the capital version of the <see cref="EACUTE_LITTLE"/> character.
         * </summary>
         */
        public static readonly char EACUTE_BIG = '\xC9';
    }
}

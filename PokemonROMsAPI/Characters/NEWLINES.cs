﻿namespace Characters
{
    /**
     * <summary>
     * Conatins all the types of newline characters and character combinations.
     * </summary>
     */
    public abstract class NEWLINES
    {
        /**
         * <summary>
         * Carriage-Return + Line Feed
         * Used by windows
         * </summary>
         */
        public static readonly string CARRIAGE_RETURN_LINE_FEED = string.Join(
            string.Empty, new char[] { '\x0D', '\x0A' }); // \r\n

        /**
         * <summary>
         * Line Feed
         * Used by Unix (OSX, Linux)
         * </summary>
         */
        public static readonly char LINE_FEED = '\x0A'; // \n

        /**
         * <summary>
         * Carriage Return
         * Used by other OSs'
         * </summary>
         */
        public static readonly char CARRIAGE_RETURN = '\x0D'; // \r

        /**
         * <summary>
         * Line Feed + Carriage Return
         * Used by other OSs'
         * </summary>
         */
        public static readonly string LINE_FEED_CARRIAGE_RETURN = string.Join(
            string.Empty, new char[] { '\x0A', '\x0D' }); // \n\r
    }
}

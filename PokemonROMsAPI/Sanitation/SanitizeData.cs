﻿using System;
using Domain.Models;

namespace Sanitation
{
    public static class SanitizeData
    {
        public static void SanitizeAllData(GameData gameData)
        {
            gameData.GameName = gameData.GameName.SanitizeXSS().SanitizeSQLInjection();
            gameData.Generation = byte.Parse(gameData.Generation.ToString().SanitizeXSS().SanitizeSQLInjection());
            gameData.Region = gameData.Region.SanitizeXSS().SanitizeSQLInjection();
            gameData.Platform = gameData.Platform.SanitizeXSS().SanitizeSQLInjection();
            gameData.DateReleased = DateTime.Parse(
                gameData.DateReleased.ToString().Replace('/', '-').SanitizeXSS().SanitizeSQLInjection());
            gameData.Description = gameData.Description.SanitizeXSS().SanitizeSQLInjection();
            gameData.CoverArtImageUrl = gameData.CoverArtImageUrl.EncodeUrl();
            gameData.WebsiteUrl = gameData.WebsiteUrl.EncodeUrl();
            gameData.RomFile.FileName = gameData.RomFile.FileName.SanitizeXSS().SanitizeSQLInjection();
            gameData.RomFile.DownloadLink = gameData.RomFile.DownloadLink.EncodeUrl();
        }
    }
}

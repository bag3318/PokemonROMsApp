﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Sanitation
{
    /**
     * <summary>
     * SQL Injection Protection Functionality
     * </summary>
     */
    public static class SQLInjection
    {
        /**
         * <summary>
         * Sanitizes data to protect from sql injections
         * </summary>
         * <param name="stringValue">the value to clean</param>
         * <returns>the sanitized sql injected value</returns>
         */
        public static string SanitizeSQLInjection(this string stringValue)
        {
            if (stringValue.Equals(null))
                return stringValue;

            Regex sqlInjectionRegex = new Regex(@"(?:(?:-{2,})|(?:[*/]+)|(?:(;|\s|)(EXEC|EXECUTE|SELECT|INSERT|UPDATE|DELETE|CREATE|ALTER|DROP|RENAME|TRUNCATE|BACKUP|RESTORE)\s))", RegexOptions.IgnoreCase);

            string newValue = string.Empty;

            string sanitizedString = sqlInjectionRegex.Replace(stringValue, newValue);

            return sanitizedString;
        }
    }
}

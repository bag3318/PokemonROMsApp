﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Sanitation
{
    /**
     * <summary>
     * URI Encoding Functionality
     * </summary>
     */
    public static class EncodeURI
    {
        /**
         * <summary>
         * Encodes any URL
         * </summary>
         * <param name="url">the url to encode</param>
         * <returns>the encoded url</returns>
         */
        public static string EncodeUrl(this string url)
        {
            if (url.Equals(null))
                return url;

            // decode url first to prevent over-encoding
            string decodedUrl = HttpUtility.UrlDecode(url);

            string encodedUrl = Uri.EscapeDataString(decodedUrl);

            return encodedUrl;
        }
    }
}

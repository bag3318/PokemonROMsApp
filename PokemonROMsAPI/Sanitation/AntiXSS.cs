﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Sanitation
{
    /**
     * <summary>
     * Cross Site Scripting (XSS) Protection Functionality
     * </summary>
     */ 
    public static class AntiXSS
    {
        /**
         * <summary>
         * Sanitizes XSS in data
         * </summary>
         * <param name="stringValue">value to sanitize</param>
         * <returns>the sanitized string</returns>
         */
        public static string SanitizeXSS(this string stringValue)
        {
            if (stringValue.Equals(null))
                return stringValue;

            // decode first to prevent over-encoding
            string decodedValue = HttpUtility.HtmlDecode(stringValue);

            string sanitizedString = HttpUtility.HtmlEncode(decodedValue);

            return sanitizedString;
        }
    }
}

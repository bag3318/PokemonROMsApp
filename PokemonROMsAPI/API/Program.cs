﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Characters;

namespace API
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Pok{0}mon ROMs API{1}", EACUTES.EACUTE_LITTLE, NEWLINES.CARRIAGE_RETURN_LINE_FEED);
            Console.ResetColor();
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, config) => config
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", 
                    optional: true, reloadOnChange: true)
                .AddEnvironmentVariables())
            .ConfigureLogging((hostingContext, logging) => logging
                .AddConfiguration(hostingContext.Configuration.GetSection("Logging"))
                .AddConsole()
                .AddDebug())
            .UseStartup<Startup>();
    }
}

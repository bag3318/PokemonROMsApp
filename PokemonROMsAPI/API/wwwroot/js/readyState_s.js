﻿//<!--
function checkAndLogDocumentReadyState_s() {
    String.prototype.capitalize = function () {
        var firstLetterOfStringToUpperCase, stringWithoutFirstLetter, capitalizedString;
        firstLetterOfStringToUpperCase = this.charAt(0).toUpperCase();
        stringWithoutFirstLetter = this.slice(1);
        capitalizedString = firstLetterOfStringToUpperCase + stringWithoutFirstLetter;
        return capitalizedString;
    };
    document.onreadystatechange = (function () {
        var readyState, readyStates, documentReadyStatePreString;
        readyState = document.readyState;
        readyStates = ["loading", "interactive", "complete"];
        documentReadyStatePreString = "Document Ready State: ";
        switch (readyState) {
            case readyStates[0]: {
                console.log(documentReadyStatePreString + readyStates[0].capitalize());
                break;
            }
            case readyStates[1]: {
                console.log(documentReadyStatePreString + readyStates[1].capitalize());
                break;
            }
            case readyStates[2]: {
                console.log(documentReadyStatePreString + readyStates[2].capitalize());
                break;
            }
            default:
                console.log(documentReadyStatePreString + readyState.capitalize());
        }
    }());
}
//-->
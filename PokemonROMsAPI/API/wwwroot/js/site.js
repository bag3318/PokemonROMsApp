﻿//<!--
jQuery(document).ready(function () {
    var APIScript = (function () {
        var logAppInfo, loadHeadingContent, imageLink, clearLocalStorage, textArt;
        function APIScript() {
            $("button").button();
            $("*[title]").tooltip();
            checkAndLogDocumentReadyState_s();
        }
        window.addEventListener("load", APIScript, true);
        logAppInfo = (function () {
            var apiInfoObj, apiInfoStr;
            apiInfoObj = {
                apiName: "Pok" + String.fromCharCode(233) + "mon ROMs API",
                apiVersion: "1.0.0.0",
                apiAuthor: "bag33188/Broccolini"
            };
            apiInfoStr = "API Name: " + apiInfoObj.apiName + ", API Version: " + apiInfoObj.apiVersion + ", API Author: " + apiInfoObj.apiAuthor + ".";
            function queryApiInfo() {
                if (!localStorage.hasOwnProperty("ApiInfo")) {
                    localStorage.setItem("ApiInfo", apiInfoStr);
                }
                return localStorage.getItem("ApiInfo");
            }
            console.log("API Info = " + queryApiInfo());
        })();
        loadHeadingContent = (function () {
            var titleHeading, apiNameHtml;
            titleHeading = $("#titleHeading");
            apiNameHtml = $("#apiName");
            if (localStorage["ApiInfo"] === undefined) {
                titleHeading.text(apiNameHtml.text());
            } else {
                var MATCH_API_NAME, apiNameRegEx;
                MATCH_API_NAME = /(?:(?:\:\x20)(?:(.*?))(?:(?=,)))/; // /(?:(?<=\:\x20)[^,]*)/
                apiNameRegEx = new RegExp(MATCH_API_NAME);
                titleHeading.text(localStorage.ApiInfo.match(apiNameRegEx)[1]); // [0]
            }
        })();
        imageLink = (function () {
            var pokemonLogoImage, eacuteHtmlEntity;
            pokemonLogoImage = $("#pokemonLogo");
            eacuteHtmlEntity = $.parseHTML("&eacute;")[0].data;
            pokemonLogoImage.attr("title", "Visit the Pok" + eacuteHtmlEntity + "mon Website!");
            pokemonLogoImage.on("click", function () {
                open("https://www.pokemon.com/", "_blank");
            });
        })();
        clearLocalStorage = (function () {
            try {
                var clearLocalStorageBtn = $("#clearLocalStorage");
                clearLocalStorageBtn.on("click", function () {
                    localStorage.clear();
                    setTimeout(function () {
                        alert("Local Storage Cleared!");
                    });
                });
            } catch (err) {
                alert("An error occured while try to clear local storage. Check the console for more info.");
                throw err;
            }
        })();
        textArt = (function () {
            function removeTextArtIfEdge() {
                var textArtWrapper, MS_EDGE, msEdgeRegEx;
                MS_EDGE = /Edge\/\d/;
                msEdgeRegEx = new RegExp(MS_EDGE, "i");
                if (msEdgeRegEx.test(navigator.userAgent)) {
                    textArtWrapper = $("#textArtWrapper");
                    textArtWrapper.hide();
                }
            }
            removeTextArtIfEdge();
            var textArtHtml, textArtData;
            textArtHtml = $("#textArt");
            textArtData = [
                "<span>&#x20;&#x5f;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x20;&#x20;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x20;</span>",
                "<br/>",
                "<span>&#x7c;&#x20;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x5f;&#x5f;&#x20;&#x2f;&#x5f;&#x2f;&#x20;&#x20;&#x5f;&#x20;&#x5f;&#x5f;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x5f;&#x20;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x7c;&#x20;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x2f;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x7c;&#x20;&#x20;&#x5c;&#x2f;&#x20;&#x20;&#x7c;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x20;&#x20;&#x20;&#x20;&#x2f;&#x20;&#x5c;&#x20;&#x20;&#x20;&#x7c;&#x20;&#x20;&#x5f;&#x20;&#x5c;&#x7c;&#x5f;&#x20;&#x5f;&#x7c;</span>",
                "<br/>",
                "<span>&#x7c;&#x20;&#x7c;&#x5f;&#x29;&#x20;&#x7c;&#x2f;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x7c;&#x20;&#x7c;&#x2f;&#x20;&#x2f;&#x2f;&#x20;&#x5f;&#x20;&#x5c;&#x7c;&#x20;&#x27;&#x5f;&#x20;&#x60;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x2f;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x7c;&#x20;&#x27;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x7c;&#x20;&#x7c;&#x5f;&#x29;&#x20;&#x7c;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x7c;&#x20;&#x7c;&#x5c;&#x2f;&#x7c;&#x20;&#x7c;&#x2f;&#x20;&#x5f;&#x5f;&#x7c;&#x20;&#x20;&#x20;&#x2f;&#x20;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x7c;&#x20;&#x7c;&#x5f;&#x29;&#x20;&#x7c;&#x7c;&#x20;&#x7c;&#x20;</span>",
                "<br/>",
                "<span>&#x7c;&#x20;&#x20;&#x5f;&#x5f;&#x2f;&#x7c;&#x20;&#x28;&#x5f;&#x29;&#x20;&#x7c;&#x7c;&#x20;&#x20;&#x20;&#x3c;&#x7c;&#x20;&#x20;&#x5f;&#x5f;&#x2f;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x7c;&#x20;&#x28;&#x5f;&#x29;&#x20;&#x7c;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x7c;&#x20;&#x20;&#x5f;&#x20;&#x3c;&#x20;&#x7c;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x7c;&#x7c;&#x20;&#x7c;&#x20;&#x20;&#x7c;&#x20;&#x7c;&#x5c;&#x5f;&#x5f;&#x20;&#x5c;&#x20;&#x20;&#x2f;&#x20;&#x5f;&#x5f;&#x5f;&#x20;&#x5c;&#x20;&#x7c;&#x20;&#x20;&#x5f;&#x5f;&#x2f;&#x20;&#x7c;&#x20;&#x7c;&#x20;</span>",
                "<br/>",
                "<span>&#x7c;&#x5f;&#x7c;&#x20;&#x20;&#x20;&#x20;&#x5c;&#x5f;&#x5f;&#x5f;&#x2f;&#x20;&#x7c;&#x5f;&#x7c;&#x5c;&#x5f;&#x5c;&#x5c;&#x5f;&#x5f;&#x5f;&#x7c;&#x7c;&#x5f;&#x7c;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x5c;&#x5f;&#x5f;&#x5f;&#x2f;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x5c;&#x5f;&#x5c;&#x20;&#x5c;&#x5f;&#x5f;&#x5f;&#x2f;&#x20;&#x7c;&#x5f;&#x7c;&#x20;&#x20;&#x7c;&#x5f;&#x7c;&#x7c;&#x5f;&#x5f;&#x5f;&#x2f;&#x20;&#x2f;&#x5f;&#x2f;&#x20;&#x20;&#x20;&#x5c;&#x5f;&#x5c;&#x7c;&#x5f;&#x7c;&#x20;&#x20;&#x20;&#x7c;&#x5f;&#x5f;&#x5f;&#x7c;</span>"
            ];
            $.each(textArtData, function (i, textData) {
                textArtHtml.append(textData);
            });
        })();
        return function api() {
            return {
                APIScript: APIScript
            };
        };
    }());
    APIScript();
});
//-->
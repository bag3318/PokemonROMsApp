﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration.Binder;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Data.Queries;
using Data.Contexts;
using Domain.IQueries;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PokemonROMsDBContext>();

            services.AddDefaultIdentity<IdentityUser>()
                .AddEntityFrameworkStores<PokemonROMsDBContext>();

            services.AddScoped<IPokemonROMsRepository, PokemonROMsRepository>();

            services.AddMvc(o => o.ReturnHttpNotAcceptable = true)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddXmlSerializerFormatters()
                .AddXmlDataContractSerializerFormatters();

            services.AddCors(options => options
                .AddPolicy("CorsPolicy", builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    //.WithOrigins(Configuration.GetSection("CORS").GetSection("AllowedHosts").Get<string[]>())
                    //.WithMethods(Configuration.GetSection("CORS").GetSection("AllowedMethods").Get<string[]>())
                    //.WithHeaders(Configuration.GetSection("CORS").GetSection("AllowedHeaders").Get<string[]>())
                    .AllowCredentials()));

            services.AddSwaggerGen(o =>
            {
                o.SwaggerDoc(Configuration.GetSection("Swagger")["Version"],
                new Info
                {
                    Title = Configuration.GetSection("Swagger")["Title"],
                    Version = Configuration.GetSection("Swagger")["Version"],
                    Description = Configuration.GetSection("Swagger")["Description"]
                });
                (string API, string Domain) xmlFilePaths = (API: "./API.xml", Domain: "../Domain/Domain.xml");
                o.IncludeXmlComments(xmlFilePaths.API);
                o.IncludeXmlComments(xmlFilePaths.Domain);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
                app.UseHsts();

            app.UseHttpsRedirection();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(o =>
                o.SwaggerEndpoint(
                    Configuration.GetSection("Swagger").GetValue<string>("EndPoint"),
                    Configuration.GetSection("Swagger").GetValue<string>("Title")));

            app.UseStaticFiles();

            app.Use(async (context, next) =>
            {
                if (context.Request.Path.Equals("/test")) await context.Response.WriteAsync("Hello World!");
                else await next();
            });
        }
    }
}

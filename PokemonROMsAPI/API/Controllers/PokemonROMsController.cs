﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Domain.Models;
using Domain.ViewModel;
using Domain.IQueries;
using Sanitation;

namespace API.Controllers
{
    /// <summary>
    /// Represents the PokemonROMsAPI Controller Group of Functionality.
    /// This controller handles the HTTP requests.
    /// </summary>
    [FormatFilter]
    [ApiController]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    public class PokemonROMsController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IPokemonROMsRepository _queries;

        private static readonly object ModelStateInvalidErrMsg = 
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 400,
                ""errorName"": ""Bad Request"",
                ""errorMessage"": ""Model State Invalid.""
            }");
        private static readonly object GameDataNullErrMsg = 
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 400,
                ""errorName"": ""Bad Request"",
                ""errorMessage"": ""No game data provided.""
            }");
        private static readonly object IdLessThanOrEqualToZeroErrMsg = 
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 400,
                ""errorName"": ""Bad Request"",
                ""errorMessage"": ""ID must be greater than 0.""
            }");
        private static readonly object RowsExistInDBTablesErrMsg =
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 400,
                ""errorName"": ""Bad Request"",
                ""errorMessage"": ""Cannot reseed tables when rows exist.""
            }");
        private static readonly object GameDataNotFoundErrMsg = 
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 404,
                ""errorName"": ""Not Found"",
                ""errorMessage"": ""Game data not found.""
            }");
        private static readonly object ExistingGameDataNotFoundErrMsg = 
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 404,
                ""errorName"": ""Not Found"",
                ""errorMessage"": ""Cannot find existing game data specified.""
            }");
        private static readonly object InvalidContentTypeErrMsg =
            JsonConvert.DeserializeObject(@"{
                ""errorCode"": 415,
                ""errorName"": ""Unsupported Media Type"",
                ""errorMessage"": ""The request''s body was in a content type that is not supported by this API.""
            }");

        public PokemonROMsController(IPokemonROMsRepository queries, ILoggerFactory logger)
        {
            _queries = queries;
            _logger = logger.CreateLogger(ToString());
        }

        // GET api/PokemonROMs
        /// <summary>
        /// Gets all Pokemon ROMs from the database.
        /// </summary>
        /// <remarks>
        /// Sample Response:
        ///
        ///     GET /api/PokemonROMs
        ///     [
        ///         {
        ///            "gameId": 30,
        ///            "gameName": "Pokemon Ranger",
        ///            "generation": 4,
        ///            "region": "Hoenn",
        ///            "dateReleased": "2006-03-23",
        ///            "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///            "platform": "Nintendo DS Lite",
        ///            "websiteUrl": "https://bit.ly/2P55V56",
        ///            "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///            "downloadLink": "https://bit.ly/2O2qvqn",
        ///            "fileName": "Pokemon Ranger.nds"
        ///         },
        ///         {
        ///             // ......
        ///     ]
        ///
        /// </remarks>
        /// <returns>A list of Pokemon ROMs. 200-ok status.</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> GetAllPokemonROMs()
        {
            _logger.LogDebug("GET Request");
            Debug.WriteLine("GET Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            List<PokemonROM> pokmeonROMsList = await _queries.ReadAllPokemonROMsAsync();
            
            return Ok(pokmeonROMsList);
        }

        // GET api/PokemonROMs/{id}
        /// <summary>
        /// Grabs a specific Pokemon ROM from the database.
        /// </summary>
        /// <remarks>
        /// Sample Response:
        ///
        ///     GET /api/PokemonROMs/30
        ///     {
        ///        "gameId": 30,
        ///        "gameName": "Pokemon Ranger",
        ///        "generation": 4,
        ///        "region": "Hoenn",
        ///        "dateReleased": "2006-03-23",
        ///        "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///        "platform": "Nintendo DS Lite",
        ///        "websiteUrl": "https://bit.ly/2P55V56",
        ///        "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///        "downloadLink": "https://bit.ly/2O2qvqn",
        ///        "fileName": "Pokemon Ranger.nds"
        ///     }
        ///
        /// </remarks>
        /// <param name="id">Represents the id of the game to grab the data for.</param>
        /// <returns>A Pokemon ROM object. 200-ok status.</returns>
        [ProducesResponseType(200)]
        [HttpGet("{id:int}.{format?}", Name = "GetPokemonROM")]
        public async Task<IActionResult> GetSinglePokemonROM([FromRoute] int id)
        {
            _logger.LogDebug("GET Request");
            Debug.WriteLine("GET Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                _logger.LogError(JsonConvert.SerializeObject(IdLessThanOrEqualToZeroErrMsg));
                Debug.WriteLine(IdLessThanOrEqualToZeroErrMsg);
                return BadRequest(IdLessThanOrEqualToZeroErrMsg);
            }

            if (!_queries.DataExists(id))
            {
                _logger.LogError(JsonConvert.SerializeObject(GameDataNotFoundErrMsg));
                Debug.WriteLine(GameDataNotFoundErrMsg);
                return NotFound(GameDataNotFoundErrMsg);
            }

            PokemonROM pokemonROM = await _queries.ReadSinglePokemonROMAsync(id);

            return Ok(pokemonROM);
        }

        // POST api/PokemonROMs
        /// <summary>
        /// Adds a Pokemon ROM to the database.
        /// </summary>
        /// <remarks>
        /// Sample Request:
        ///
        ///     POST /api/PokemonROMs
        ///     {
        ///        "gameName": "Pokemon Ranger",
        ///        "generation": 4,
        ///        "region": "Hoenn",
        ///        "dateReleased": "2006-03-23",
        ///        "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///        "platform": "Nintendo DS Lite",
        ///        "websiteUrl": "https://bit.ly/2P55V56",
        ///        "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///        "romFile": {
        ///             "downloadLink": "https://bit.ly/2O2qvqn",
        ///             "fileName": "Pokemon Ranger.nds"
        ///        }
        ///     }
        ///
        /// Sample Response:
        ///
        ///     {
        ///        "gameId": 30,
        ///        "gameName": "Pokemon Ranger",
        ///        "generation": 4,
        ///        "region": "Hoenn",
        ///        "dateReleased": "2006-03-23",
        ///        "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///        "platform": "Nintendo DS Lite",
        ///        "websiteUrl": "https://bit.ly/2P55V56",
        ///        "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///        "downloadLink": "https://bit.ly/2O2qvqn",
        ///        "fileName": "Pokemon Ranger.nds"
        ///     }
        ///
        /// </remarks>
        /// <param name="gameData">Represents the game data to add to the database.</param>
        /// <returns>A Pokemon ROM object. 201-created status.</returns>
        [HttpPost]
        [ProducesResponseType(201)]
        public async Task<IActionResult> PostPokemonROM([FromBody] GameData gameData)
        {
            _logger.LogDebug("POST Request");
            Debug.WriteLine("POST Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            if (gameData == null)
            {
                _logger.LogError(JsonConvert.SerializeObject(GameDataNullErrMsg));
                Debug.WriteLine(GameDataNullErrMsg);
                return BadRequest(GameDataNullErrMsg);
            }

            if (Request.Headers["Content-Type"] != "application/json" &&
                Request.Headers["Content-Type"] != "application/xml")
            {
                _logger.LogError(JsonConvert.SerializeObject(InvalidContentTypeErrMsg));
                Debug.WriteLine(InvalidContentTypeErrMsg);
                return new UnsupportedMediaTypeResult();
            }

            SanitizeData.SanitizeAllData(gameData);

            PokemonROM pokemonROM = await _queries.CreatePokemonROMAsync(gameData);

            return CreatedAtRoute("GetPokemonROM", new { Controller = "PokemonROMs", id = gameData.Id }, pokemonROM);
        }

        // POST api/PokemonROMs/Core
        /// <summary>
        /// Adds the default core Pokemon ROMs to the database and deletes everything else first.
        /// </summary>
        /// <remarks>
        /// Sample Response:
        /// 
        ///     POST /api/PokemonROMs/Core
        ///     [
        ///         {
        ///            "gameId": 30,
        ///            "gameName": "Pokemon Ranger",
        ///            "generation": 4,
        ///            "region": "Hoenn",
        ///            "dateReleased": "2006-03-23",
        ///            "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///            "platform": "Nintendo DS Lite",
        ///            "websiteUrl": "https://bit.ly/2P55V56",
        ///            "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///            "romFile": {
        ///                 "downloadLink": "https://bit.ly/2O2qvqn",
        ///                 "fileName": "Pokemon Ranger.nds"
        ///            }
        ///         },
        ///         // ......
        ///     ]
        ///     
        /// </remarks>
        /// <returns>A list of all the core Pokemon ROMs. 200-ok status.</returns>
        [HttpPost]
        [Route("Core")]
        [ProducesResponseType(201)]
        public async Task<IActionResult> PostCorePokemonROMs()
        {
            _logger.LogDebug("POST Request");
            Debug.WriteLine("POST Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            List<PokemonROM> corePokemonROMsList = await _queries.CreateCorePokemonROMsAsync();

            return CreatedAtAction("PostCorePokemonROMs", corePokemonROMsList);
        }

        // PUT api/PokemonROMs/{id}
        /// <summary>
        /// Updates a Pokemon ROM in the database.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/PokemonROMs/30
        ///     {
        ///        "gameName": "Pokemon Ranger",
        ///        "generation": 4,
        ///        "region": "Hoenn",
        ///        "dateReleased": "2006-03-23",
        ///        "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///        "platform": "Nintendo DS Lite",
        ///        "websiteUrl": "https://bit.ly/2P55V56",
        ///        "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///        "romFile": {
        ///             "downloadLink": "https://bit.ly/2O2qvqn",
        ///             "fileName": "Pokemon Ranger.nds"
        ///        }
        ///     }
        ///
        /// Sample Response:
        ///
        ///     {
        ///        "gameId": 30,
        ///        "gameName": "Pokemon Ranger",
        ///        "generation": 4,
        ///        "region": "Hoenn",
        ///        "dateReleased": "2006-03-23",
        ///        "description": "Tame Pokemon as a Ranger and recieve a special prize at the end of the game.",
        ///        "platform": "Nintendo DS Lite",
        ///        "websiteUrl": "https://bit.ly/2P55V56",
        ///        "coverArtImageUrl": "https://bit.ly/2NkttkO",
        ///        "downloadLink": "https://bit.ly/2O2qvqn",
        ///        "fileName": "Pokemon Ranger.nds"
        ///     }
        ///
        /// </remarks>
        /// <param name="id">Represents the id of the game data to update.</param>
        /// <param name="gameData">Represents the updated game data.</param>
        /// <returns>A Pokemon ROM object. 200-ok status.</returns>
        [HttpPut("{id:int}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> PutPokemonROM([FromRoute] int id, [FromBody] GameData gameData)
        {
            _logger.LogDebug("PUT Request");
            Debug.WriteLine("PUT Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                _logger.LogError(JsonConvert.SerializeObject(IdLessThanOrEqualToZeroErrMsg));
                Debug.WriteLine(IdLessThanOrEqualToZeroErrMsg);
                return BadRequest(IdLessThanOrEqualToZeroErrMsg);
            }

            if (gameData == null)
            {
                _logger.LogError(JsonConvert.SerializeObject(GameDataNullErrMsg));
                Debug.WriteLine(GameDataNullErrMsg);
                return BadRequest(GameDataNullErrMsg);
            }

            if (!_queries.DataExists(id))
            {
                _logger.LogError(JsonConvert.SerializeObject(ExistingGameDataNotFoundErrMsg));
                Debug.WriteLine(ExistingGameDataNotFoundErrMsg);
                return NotFound(ExistingGameDataNotFoundErrMsg);
            }

            if (Request.Headers["Content-Type"] != "application/json" &&
                Request.Headers["Content-Type"] != "application/xml")
            {
                _logger.LogError(JsonConvert.SerializeObject(InvalidContentTypeErrMsg));
                Debug.WriteLine(InvalidContentTypeErrMsg);
                return new UnsupportedMediaTypeResult();
            }

            SanitizeData.SanitizeAllData(gameData);

            PokemonROM pokemonROM = await _queries.UpdatePokemonROMAsync(id, gameData);

            return Ok(pokemonROM);
        }

        // DELETE api/PokemonROMs/{id}
        /// <summary>
        /// Deletes a single Pokemon ROM from the database.
        /// </summary>
        /// <remarks>
        /// Sample Response:
        /// 
        ///     DELETE /api/PokemonROMs/30 
        ///     {
        ///         "deletedDataId": 30,
        ///         "successfullyDeletedData": true
        ///     }
        ///     
        /// </remarks>
        /// <param name="id">Represents the id of the game and rom data to delete.</param>
        /// <returns>Succesfully deleted object. 200-ok status.</returns>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeletePokemonROM([FromRoute] int id)
        {
            _logger.LogDebug("DELETE Request");
            Debug.WriteLine("DELETE Request");
            
            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            if (id <= 0)
            {
                _logger.LogError(JsonConvert.SerializeObject(IdLessThanOrEqualToZeroErrMsg));
                Debug.WriteLine(IdLessThanOrEqualToZeroErrMsg);
                return BadRequest(IdLessThanOrEqualToZeroErrMsg);
            }

            if (!_queries.DataExists(id))
            {
                _logger.LogError(JsonConvert.SerializeObject(GameDataNotFoundErrMsg));
                Debug.WriteLine(GameDataNotFoundErrMsg);
                return NotFound(GameDataNotFoundErrMsg);
            }

            await _queries.RemovePokemonROMAsync(id);

            object singleDataDeletedSuccessObject = new { deletedDataId = id, successfullyDeletedData = true };

            return Ok(singleDataDeletedSuccessObject);
        }

        // DELETE api/PokemonROMs
        /// <summary>
        /// Deletes all Pokemon ROMs in the database.
        /// </summary>
        /// <remarks>
        /// Sample Response:
        /// 
        ///     DELETE /api/PokemonROMs 
        ///     {
        ///         "successfullyDeletedAllData": true
        ///     }
        ///     
        /// </remarks>
        /// <returns>Succesfully deleted object. 200-ok status.</returns>
        [HttpDelete]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteAllPokemonROMs()
        {
            _logger.LogDebug("DELETE Request");
            Debug.WriteLine("DELETE Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            await _queries.RemoveAllPokemonROMsAsync();

            object allDataDeletedSuccessObject = new { successfullyDeletedAllData = true };

            return Ok(allDataDeletedSuccessObject);
        }

        // PATCH api/PokemonROMs
        /// <summary>
        /// Reseeds all the tables in the database so that the primary key, ID, will be reseeded back to 1.
        /// </summary>
        /// <remarks>
        /// This method does not return any content.
        /// </remarks>
        /// <returns>No content. 204-no-content status.</returns>
        [HttpPatch]
        [ProducesResponseType(204)]
        public async Task<IActionResult> PatchReseedDBTables()
        {
            _logger.LogDebug("PATCH Request");
            Debug.WriteLine("PATCH Request");

            if (!ModelState.IsValid)
            {
                _logger.LogError(JsonConvert.SerializeObject(ModelStateInvalidErrMsg));
                Debug.WriteLine(ModelStateInvalidErrMsg);
                return BadRequest(ModelState);
            }

            if (!_queries.DataExists())
            {
                await _queries.PartialUpdateReseedTablesAsync();
            }
            else
            {
                _logger.LogError(JsonConvert.SerializeObject(RowsExistInDBTablesErrMsg));
                Debug.WriteLine(RowsExistInDBTablesErrMsg);
                return BadRequest(RowsExistInDBTablesErrMsg);
            }

            return StatusCode(204);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Reflection;
using System.Xml.XPath;
using Microsoft.AspNetCore.Mvc;
using Characters;

namespace API.Controllers
{
    /// <summary>
    /// Represents the Home Page Controller.
    /// This renders the <see cref="Index"/> razor-page.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Renders the Index View.
        /// </summary>
        /// <returns>The Page View.</returns>
        public IActionResult Index()
        {
            ViewData["API Name"] = $"Pok{EACUTES.EACUTE_LITTLE}mon ROMs API";
            ViewBag.creator = new XPathDocument($"{Assembly.GetEntryAssembly().GetName().Name}.csproj")
                .CreateNavigator()
                .SelectSingleNode("/Project/PropertyGroup/Authors/text()")
                .Value;

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    /// <summary>
    /// Represents the `GameData` table in the db
    /// </summary>
    [Table("GameData")]
    public partial class GameData
    {
        public GameData()
        {
            RomFile = new RomFile();
        }

        /// <summary>
        /// Represents the ID of the game.
        /// </summary>
        [Key]
        [Column("ID", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Represents the name of the game.
        /// </summary>
        [Required]
        [StringLength(44)]
        [Column("GameName", TypeName = "NVARCHAR(44)")]
        public string GameName { get; set; }

        /// <summary>
        /// Represents the game's Pokemon generation.
        /// </summary>
        [Required]
        [Range(1, 8)]
        [Column("Generation", TypeName = "TINYINT")]
        public byte Generation { get; set; }
        
        /// <summary>
        /// Represents the region of the Pokemon game.
        /// </summary>
        [Required]
        [StringLength(23)]
        [Column("Region", TypeName = "NVARCHAR(23)")]
        public string Region { get; set; }

        /// <summary>
        /// Represents the date the game was released.
        /// </summary>
        [Required]
        [Column("DateReleased", TypeName = "DATE")]
        public DateTime DateReleased { get; set; }

        /// <summary>
        /// Represents the game's playing console.
        /// </summary>
        [Required]
        [StringLength(23)]
        [Column("Platform", TypeName = "NVARCHAR(23)")]
        public string Platform { get; set; }

        /// <summary>
        /// Represents the game's homepage website on Pokemon.com.
        /// </summary>
        [Required]
        [StringLength(400)]
        [Column("WebsiteURL", TypeName = "NVARCHAR(400)")]
        public string WebsiteUrl { get; set; }

        /// <summary>
        /// Rerpesents Pokemon's official description for the game.
        /// </summary>
        [Required]
        [StringLength(8000)]
        [Column("Description", TypeName = "NVARCHAR(MAX)")]
        public string Description { get; set; }

        /// <summary>
        /// Represents the game's cover art image url.
        /// </summary>
        [Required]
        [StringLength(400)]
        [Column("CoverArtImageURL", TypeName = "NVARCHAR(400)")]
        public string CoverArtImageUrl { get; set; }

        /// <summary>
        /// Represents the ROM File data for the game.
        /// </summary>
        [InverseProperty("GameData")]
        public RomFile RomFile { get; set; }
    }
}

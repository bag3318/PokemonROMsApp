﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    /// <summary>
    /// Represents the `ROMFiles` table in the db
    /// </summary>
    [Table("ROMFiles")]
    public partial class RomFile
    {
        /// <summary>
        /// Reprents the primary key (ID) for the game.
        /// </summary>
        [Key]
        [Column("ID", TypeName = "INT")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Reprents the game's foriegn key (ID); it's connected to the primary key of the `Game Data` table.
        /// </summary>
        [Column("GameID", TypeName = "INT")]
        public int GameId { get; set; }

        /// <summary>
        /// Represents the ROM's file name.
        /// </summary>
        [Required]
        [StringLength(50)]
        [Column("FileName", TypeName = "NVARCHAR(50)")]
        public string FileName { get; set; }

        /// <summary>
        /// Represents the download link for the Pokemon Game's ROM
        /// </summary>
        [Required]
        [StringLength(400)]
        [Column("DownloadLink", TypeName = "NVARCHAR(400)")]
        public string DownloadLink { get; set; }

        /// <summary>
        /// Represents the game data for the ROM file
        /// </summary>
        [ForeignKey("GameId")]
        [InverseProperty("RomFile")]
        public GameData GameData { get; set; }
    }
}

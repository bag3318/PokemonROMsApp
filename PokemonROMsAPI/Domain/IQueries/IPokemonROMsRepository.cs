﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;
using Domain.ViewModel;

namespace Domain.IQueries
{
    public interface IPokemonROMsRepository
    {
        bool DataExists(int id = 0);
        Task<List<PokemonROM>> ReadAllPokemonROMsAsync();
        Task<PokemonROM> ReadSinglePokemonROMAsync(int id);
        Task<PokemonROM> CreatePokemonROMAsync(GameData gameData);
        Task<List<PokemonROM>> CreateCorePokemonROMsAsync();
        Task<PokemonROM> UpdatePokemonROMAsync(int id, GameData gameData);
        Task RemovePokemonROMAsync(int id);
        Task RemoveAllPokemonROMsAsync();
        Task PartialUpdateReseedTablesAsync();
    }
}

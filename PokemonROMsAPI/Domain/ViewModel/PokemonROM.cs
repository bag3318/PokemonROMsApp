﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.ViewModel
{
    /// <summary>
    /// Represents the domain layer's View Model
    /// This is an extension of the <see cref="Models.RomFile"/> and <see cref="Models.GameData"/> object models
    /// </summary>
    public sealed class PokemonROM
    {
        /// <summary>
        /// Reprents the game's ID as a PK.
        /// </summary>
        [Key]
        [Column("GameID")]
        public int GameId { get; set; }

        /// <summary>
        /// Represents the name of the game.
        /// </summary>
        [Column("GameName")]
        public string GameName { get; set; }

        /// <summary>
        /// Represents the game's Pokemon generation.
        /// </summary>
        [Column("Generation")]
        public byte Generation { get; set; }

        /// <summary>
        /// Represents the game's region.
        /// </summary>
        [Column("Region")]
        public string Region { get; set; }

        /// <summary>
        /// Represents the date the game was released.
        /// </summary>
        [Column("DateReleased")]
        public DateTime DateReleased { get; set; }

        /// <summary>
        /// Rerpesents Pokemon's official description for the game.
        /// </summary>
        [Column("Description")]
        public string Description { get; set; }

        /// <summary>
        /// Represents the game's playing console.
        /// </summary>
        [Column("Platform")]
        public string Platform { get; set; }

        /// <summary>
        /// Represents the game's homepage website on Pokemon.com.
        /// </summary>
        [Column("WebsiteURL")]
        public string WebsiteUrl { get; set; }

        /// <summary>
        /// Represents the game's cover art image url.
        /// </summary>
        [Column("CoverArtImageURL")]
        public string CoverArtImageUrl { get; set; }

        /// <summary>
        /// Represents the download link for the Pokemon Game's ROM
        /// </summary>
        [Column("DownloadLink")]
        public string DownloadLink { get; set; }

        /// <summary>
        /// Represents the ROM's file name.
        /// </summary>
        [Column("FileName")]
        public string FileName { get; set; }
    }
}

[comment]: # (BEGIN README.md)

PokemonROMsApp
==============

![Pokemon Logo](https://bit.ly/2z0E5Rt)

## About

>>>
This Repository is a **Pokemon ROMs Full Stack Web Application and Web Database** that contains all the Core Pokemon Game ROMs.

Each ROM was directly dumped from their respective cartridges and they will all have their own (direct) download link.
>>>

### Cartridge Dumping Resources

All cartridges (except _Pokemon Green_) were bought from [Gamestop.com](https://www.gamestop.com).

The **Pokemon Green** cartridge was bought [here](http://a.co/d/2P4J4MG).

#### GB/GBC Cartridges

- [Reader/Writer Gen(s) 1/2 - The joey-joebags](https://bennvenn.myshopify.com/products/reader-writer-gen2)
- [Software For Reader](http://www.no-intro.org/tools.htm)

#### GBA Cartridges

- [R43DS Flashcart](https://www.r43ds.org/products/R4-3DS.html)
- [R4/R43DS Homebrew GBA Backup Tool App](https://projectpokemon.org/home/applications/core/interface/file/attachment.php?id=31122)

#### NDS/3DS Cartridges

- [CFW](https://3ds.hacks.guide/)
- [Decrypt9](https://github.com/d0k3/Decrypt9WIP/releases)

## Compatibility

This full-stack-web-app is compatible with Windows, Mac/OSX, and Linux.

Below is a table that shows the minimum version requirements for running this application.

| Windows | OSX/Mac        | Linux                     |
|---------|----------------|---------------------------|
| 7 (x64) | Yosemite (x64) | Latest Stable Build (x64) |

> Note: Windows 8 RT, Windows 8.1 RT, and Windows 10 S will not be able to run this application.

## Development Tools

For this overall application at the front-most level, I used [Sublime Text 3](https://www.sublimetext.com/3 "Download Sublime Text 3"), which is a lightweight text-editor. 

Open the `pokemon-roms-app.sublime-project` file with **Sublime Text** to view the full application/structure.

## Samples

In the `samples` folder (located in the root directory of this repo) contains a `sample.xml` and a `sample.json` file for use of posting to the backend.

Within these files are the proper data formats you would need to send when doing a `POST` or `PUT` request to the server-side, and the sample responses when doing a method that returns data (such as a `GET` method).

## Software Used

* [Git][]
* [Cmder][]
* [Draw\.io][]
* [Postman][]
* [Fiddler][]
* [Sublime Text 3][]
* [Visual Studio Code][]
* [Visual Studio 2017 Community Edition][]
* [SQL Server Express 2017][]
* [SQL Server Managment Studio][]
* [Node\.JS \(Runtime\)][]

[Git]: <https://git-scm.com/downloads> "Download Git SCM"
[Cmder]: <http://cmder.net/> "Download Cmder"
[Draw\.io]: <https://about.draw.io/integrations/#integrations_offline> "Download Draw.io Offline"
[Postman]: <https://www.getpostman.com/apps> "Download Postman"
[Fiddler]: <https://www.telerik.com/download/fiddler> "Download Fiddler"
[Sublime Text 3]: <https://www.sublimetext.com/3> "Download Sublime Text 3"
[Visual Studio Code]: <https://code.visualstudio.com/download> "Download Visual Studio Code"
[Visual Studio 2017 Community Edition]: <https://visualstudio.microsoft.com/vs/community/> "Download Visual Studio 2017 Community Edition"
[SQL Server Express 2017]: <https://www.microsoft.com/en-us/sql-server/sql-server-editions-express> "Download SQL Server Express 2017"
[SQL Server Managment Studio]: <https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017> "Download SQL Server Managment Studio"
[Node\.JS \(Runtime\)]: <https://nodejs.org/en/download/> "Download Node\.JS (Runtime)"

## Important Links

* [Full ROMs Download Link](https://1drv.ms/f/s!AmuGEkjVxD0JgtYGGtGKZRVovIEIcA)
* [Repo Images Download Link](https://bit.ly/2ROXGeK)

## URLs

* **https://<span></span>localhost:4438** - API
* **http://<span></span>localhost:4200** - Angular App

________

> _**Enjoy!**_

[comment]: # "END README.md"